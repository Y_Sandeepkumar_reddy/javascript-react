// function UserGreeting(props) {
//   if (props.isLoggedIn) {
//     return <h2>welcome {props.username}</h2>;
//   }
// return <h2>please login to continue</h2>
  
// }
// export default UserGreeting;


import propType from 'prop-types'
function UserGreeting(props) {

  const welcomeMessage=<h2 className="welcome-message">welcome {props.username}</h2>;
  const logPrompt =<h2 className="login-prompt">please login to continue</h2>;
    return  (props.isLoggedIn ? welcomeMessage:
                                logPrompt)
}

UserGreeting.prototype={
isLoggedIn:propType.bool,
username:propType.string,
}
UserGreeting.defaultProps={
   isLoggedIn:'false',
   username:"Guist",
}

export default UserGreeting;
