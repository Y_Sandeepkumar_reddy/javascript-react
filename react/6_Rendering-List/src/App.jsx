import List from "./List.jsx"
const fruits=[
  {id:1,name:'apple', calories:101}, 
  {id:2,name:'banana', calories:45},
  {id:3,name:"carrot", calories:105},
  {id:4,name:"orange", calories:45},
  {id:5,name:"mango", calories:37}
];
const vegetables=[
  {id:6,name:'apple', calories:101}, 
  {id:7,name:'banana', calories:45},
  {id:8,name:"carrot", calories:105},
  {id:9,name:"orange", calories:45},
  {id:10,name:"mango", calories:37}
];

function App() {
  return (
    <>
           {fruits.length > 0 ? < List  items={fruits} category={"fruits"}/>:null}
           {vegetables.length > 0 ? < List  items={vegetables} category={"vegetables"}/> : null}
    </>
  )
}

export default App
