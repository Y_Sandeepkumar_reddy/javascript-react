import React from 'react'

function Button() {
    const style={
        
            backgroundColor: 'rgb(21, 46, 187)',
            color: 'white',
            borderRadius: '5px',
            padding: '10px,20px',
            cursor: 'pointer',
            border: 'none',
            height: '70px',
            width: '100px'
            
    }
  return (
    <button style={style} className='button'>
      click me
    </button>
  )
}

export default Button
