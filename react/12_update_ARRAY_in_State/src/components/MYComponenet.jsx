import React from 'react';
import { useState } from 'react';

function MYComponenet() {
    const[foods,setFood]=useState(["apple","banana","mango","grapes"]);
    const handleFood=()=>{
        const newFood=document.getElementById("foodInput").value ;
        document.getElementById("foodInput").value ="";

        setFood(foods=>[...foods,newFood]);
    }
    const handleRemoveFood=(index)=>{
   setFood(foods.filter((_,i)=>i!==index))
    }
  return (
    <div>
        <h2> i like most : </h2>
        <ul>
            { 
                foods.map((food,index) =>{
                    return <li key={index} onClick={()=>handleRemoveFood(index)}>{food}  </li>
                })
            }<br/>
            <input type="text" id='foodInput' placeholder='enter your fav food' />
            <button onClick={handleFood}>Add Food</button>
        </ul>
    </div>
  )
}

export default MYComponenet
