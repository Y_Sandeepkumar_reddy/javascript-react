//! onChange  =  event handler is used primarily with from elements
//!               ex : <input>     <textarea>     <select>      <radio>
//!               triggers a function every time the value of the input changes.

import ColorPicker from "./ColorPicker.jsx"
import Java from "./Java.jsx"
import MyComponent from "./MyComponent.jsx"

//?

function App() {

  return (
    <>
    <MyComponent/>
    < ColorPicker/> 
  
    </>
  )
}

export default App
