import React from 'react'
import { useState } from 'react'

function MyComponent() {

    const [name, setName]=useState("");
    const [quantity,setQuantity]=useState(0);
    const [comment,setComment]=useState("");
    const [payment,setPayment]=useState("");
    const [shipping,setShipping]=useState("");

    const handleName=(event)=>{
        setName(event.target.value);
    }
    const handleQuanty=(event)=>{
        setQuantity(event.target.value);
    }
    const handleComment=(event)=>{
        setComment(event.target.value);
    }
    const handlePayment=(event)=>{
        setPayment(event.target.value);
    }
    const handleShipping=(event)=>{
        setShipping (event.target.value);
    }
  return (
    <div className='main-div'>
        <h4>enter some data</h4>
      <div><input type="text" value={name} onChange={handleName} required  placeholder='enter your lover name'/>
      <p> you entered data is : {name}</p></div>
      
      <input type="number" value={quantity} onChange={handleQuanty} min={0} placeholder='enter the quantity'  />

      <h4>please comment</h4>
      <textarea value={comment} onChange={handleComment} placeholder='enter delevery instructions'></textarea>
      <p>your Comment is: {comment}</p>
      <h4>payment</h4>
      <select onChange={handlePayment}>
        <option value="">select an option </option>
        <option value="visa">Visa</option>
        <option value="masterCard">MasterCard</option>
        <option value="Rupy">Rupy</option>
        <option value="Cred">Cred</option>
      </select>
      <p>your payment option is :{payment}</p>
      <h4>shipping status</h4>
      <label>
         <input type="radio" value={"pickup"} checked={shipping === "pickup"} onChange={handleShipping} />
         PickUp
      </label>
      <label>
        <br />
      <input type="radio" value={"Delivary"} checked={shipping === "Delivery"} onChange={handleShipping} />
        Delivery 
        <p>shipping :{shipping}</p>
      </label>
    </div>
  )
}

export default MyComponent
