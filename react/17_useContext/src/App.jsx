//!      useContext()     =  React Hook that allows you to share values between multiple levels of Components,

import Component1 from "./components/Component1.jsx"

//!                          without passing prop through each level 


//?       first Component1 folder is  wrong approach so use component2 folder

//!  PROVIDER COMPONENT :
//? 1. import {createContext} from 'react';
//? 2. export const MyContext = createcontext();
//? 3. <MyContext.Provider value={value}>
//?       <Child>
//?     </MyContext.Provider>

//!  CONSUMER COMPONENT
//? 1. import React,{useContext} fronm 'react';
//?       import {MYContext} from './Component1`;
//? 2. const value=useContext(MyContext); 
function App() {

  return (
    <>
    <Component1/>
    </>
  )
}

export default App
