import React,{useState,createContext} from 'react'
import Component2 from './Component2.jsx'

export const  UserContext= createContext();

function Component1() {
    const [user,setUser]=useState("sandheep");

  return (<>
    <div className='box'>
        <h1>Component1</h1>
        <h2>{`Hello ${user}`}</h2>

       <UserContext.Provider value={user}>
             <Component2  user={user}/>
        </UserContext.Provider> 
    </div>

    </>)
}

export default Component1