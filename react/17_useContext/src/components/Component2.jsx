import React from 'react'
import Component3 from './Component3'

function Component2(props) {
  return (
    <div className='box'>
        <h1>Component2</h1>
        <Component3 user={props.user}/>
    </div>
  )
}

export default Component2