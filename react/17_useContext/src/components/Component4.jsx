import React,{useContext} from 'react'
import {UserContext} from "./Component1.jsx";


function Component4() {
    const user = useContext(UserContext);

  return (<>
    <div className='box'>
        <h1>Component4</h1>
        <h2>{`BYE ${user}`}</h2>

    </div>
   
    </>)
}

export default Component4