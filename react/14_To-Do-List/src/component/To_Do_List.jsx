import React, { useState } from 'react';

function To_Do_List() {
    const [task, setTask] = useState(["banana"]);
    const [newTask, setNewTask] = useState("");

    const handleInputChange = (event) => {
        setNewTask(event.target.value);
    };

    const addTask = () => {
        if (newTask.trim() !== "") {
            setTask(task => [...task, newTask]);
            setNewTask("");
        }
    };

    const deleteTask = (index) => {
        const update = task.filter((_, i) => i !== index);
        setTask(update);
    };

    const moveTaskUp = (index) => {
        if (index === 0) return; // Don't move if it's the first task
        const updateTask = [...task];
        [updateTask[index], updateTask[index - 1]] =
            [updateTask[index - 1], updateTask[index]];
        setTask(updateTask);
    };

    const moveTaskDown = (index) => {
        if (index === task.length - 1) return; // Don't move if it's the last task
        const updateTask = [...task];
        [updateTask[index], updateTask[index + 1]] =
            [updateTask[index + 1], updateTask[index]];
        setTask(updateTask);
    };

    return (
        <div className='to-Do-List'>
            <h1>To-Do-List</h1>
            <div className='main'>
                <input type="text" placeholder='Add a task' onChange={handleInputChange} value={newTask} />
                <button className='add-button' onClick={addTask}>
                    ADD
                </button>
            </div>
            <ol>
                {task.map((task, index) => (
                    <li key={index}>
                        <span className='text'>
                            {task}
                        </span>
                        <button className='delete-button' onClick={() => deleteTask(index)}>Delete</button>
                        <button className='move-button' onClick={() => moveTaskUp(index)}>Up</button>
                        <button className='move-button' onClick={() => moveTaskDown(index)}>Down</button>
                    </li>
                ))}
            </ol>
        </div>
    );
}

export default To_Do_List;
