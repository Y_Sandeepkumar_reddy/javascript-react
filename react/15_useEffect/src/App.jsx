//!         useEffect() = React HOOK that tells React DO SOME WHEN (Pick one):
//!                       This component re-render
//!                       This component mounts
//!                      The state of a value

//?      useEffect(function,[dependencies])

//?        1 . useEffect(()=>{})            runs after every re-render .
//?        2.  useEffect(()=>{},[])         runs only on mount  .
//?        3.  useEffect(()=>{},[value])    runs on mount + when value changes .

//Todo      USES
//?        1. Event Listeners .
//?        2. DOM manipulation .
//?        3. Subscriptions (real-time updates) .
//?        4. Fetching data from an API .
//?        5. Clean up when an component unmount .

import Component2 from "./comoponents/Component2";
import MYComponent from "./comoponents/MYComponent";

function App() {
  return (
    <>
      <MYComponent />
      < Component2 />
    </>
  );
}

export default App;
