import React, { useState } from 'react'
import { useEffect } from 'react';

function MYComponent() {
    const[count,setCount]=useState(0);
    const[color,setColor]=useState("green")
    useEffect(()=>{
        document.title=`sandeep ${count} ${color}`;
    },[count,color])
    const handleCount=()=>{
        setCount(count=>count+1);
    }
    const handleSub=()=>{
       setCount(c=>c-1)
    }
    function handleColor() {
        setColor(c=>c==="green"?"red":"green")
    }

  return (
    <>
       <p style={{color}}>count :{count}</p>
       <button onClick={handleCount}>
            ADD
       </button>
       <button onClick={handleSub}>Sub</button> <br />
       <button onClick={handleColor}> Change Color</button>
    </>
  )
}

export default MYComponent