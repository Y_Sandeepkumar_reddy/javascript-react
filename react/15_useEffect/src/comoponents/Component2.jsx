import React, { useState, useEffect } from 'react';

function Component2() {
    const [width, setWidth] = useState(window.innerWidth);
    const [height, setHeight] = useState(window.innerHeight);

    useEffect(() => {
        const handleResize = () => {
            setWidth(window.innerWidth);
            setHeight(window.innerHeight);
        };

        window.addEventListener("resize", handleResize);

        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []); // Empty dependency array means this effect will only run once after the component mounts

    useEffect(() => {
        document.title = `size : ${width} X ${height}`;
    }, [width, height]);

    return (
        <>
            <br />
            <p>Width: {width} px</p>
            <p>Height: {height} px</p>
        </>
    );
}

export default Component2;
