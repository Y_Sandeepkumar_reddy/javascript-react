//?  PROPS  = Read only properties that  are shared between components.
//?           A  parent component can send data to a child component .
//!   -->  React Props are like function arguments in JavaScript and attributes in HTML.To send props into a component, use the same syntax as HTML attributes:
//?            < Component  key= value > => const myElement = <Car brand="Ford" /> 






import Student from "./Student.jsx"
function App() {

  return (
    <>
    <Student name='Sandheep' age={23} gender="M" isStudent={true} />
    <Student name='kumar' age="30" gender="M" isStudent={false} />
    <Student name='mounika' age={22} gender="F" isStudent={true} />
    <Student name='Sandhe' age={24} gender="M" isStudent={true} />

    <Student/>

    </>
  )
}

export default App


//? propTypes  -->  A mechanism that ensures that the passed value is of the correct data type.
//?       age : propType.number

//? defaultProps   --> default values for props in cause they are not passed from the parent element    name : "Guist"