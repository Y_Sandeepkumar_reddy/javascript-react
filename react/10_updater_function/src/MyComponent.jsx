//! update function = A function passed as an argument to setState() usually
//!                   ex : setYear(y=>y+1)
//!     allows for safe updates based on the previous state.
//!     used with multiple state updates and asynchronous function .
//!     good practice to use updater function .

import React, { useState } from "react";

function MyComponent() {
  const [count, setCount] = useState(0);

  function increment() {
//? Takes the PENDING state to calculate NEXT State .
//? React puts your updater function in a queue (waiting in aline) .
//? During the next render , it will call them in a same order .
    setCount(c=>c + 1);
    setCount(c=>c + 1);  //correct way

  }
  function decrement() {
    setCount(c=>c - 1);
    // setCount(c=>c - 1);      correct way 

  }
  function reset() {
    setCount(c=>c=0);
  }

  return (
    <div>
      <p>count : {count}</p>
      <button onClick={decrement}>Decrement</button>
      <button onClick={reset}>Reset</button>
      <button onClick={increment}>Increment</button>
    </div>
  );
}

export default MyComponent;
