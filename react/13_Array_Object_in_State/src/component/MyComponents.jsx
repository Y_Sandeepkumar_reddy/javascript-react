import React, { useState } from 'react';

function MyComponents() {
  const [car, setCar] = useState([]);
  const [carYear, setCarYear] = useState(new Date().getFullYear());
  const [carMake, setCarMake] = useState('');
  const [carModel, setCarModel] = useState('');

  const handleAddCar = () => {
    // Check if any field is empty
    if (!carYear || !carMake || !carModel) {
      alert('Please fill out all fields');
      return; // Exit function early if any field is empty
    }

    // All fields are filled, proceed to add car
    const newCar = { year: carYear, make: carMake, model: carModel };
    setCar([...car, newCar]);
  };

  const handleRemoveCar = (index) => {
    setCar(car=>car.filter((_,i)=> i!== index));
  };

  const handleYearChange = (event) => {
    setCarYear(event.target.value);
  };

  const handleMakeChange = (event) => {
    setCarMake(event.target.value);
  };

  const handleModelChange = (event) => {
    setCarModel(event.target.value);
  };

  return (
    <div>
      <h2>List of Car Objects</h2>
      <ul>
        {car.map((car, index) => (
          <li key={index} onClick={()=>handleRemoveCar(index)}>
            {car.year} {car.make} {car.model}
          </li>
        ))}
      </ul>
      <input type="number" value={carYear} onChange={handleYearChange} />
      <br />
      <input type="text" onChange={handleMakeChange} placeholder="Enter car Make" required />
      <br />
      <input type="text" onChange={handleModelChange} placeholder="Enter Car Model" required />
      <br />
      <button onClick={handleAddCar}>Add Car</button>
    </div>
  );
}

export default MyComponents;
