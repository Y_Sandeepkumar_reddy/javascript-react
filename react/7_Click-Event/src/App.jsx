//! Click-Event  --> An interaction when a user click on the specific element.
//!                  we can respond to clicks by passing  a callback to the  onClick event handler

import Button from "./Button.jsx"
import Profile from "./Profile.jsx"

function App() {

  return (
    <>
    < Button />
    <Profile/>
    </>
  )
}

export default App
