import React, { useState } from "react";

function MyComponent() {
  const [name, setName] = useState(" your name please"); // Initialize with an empty string or any other default value
  const [age,setAge]=useState(0)
  const [isEmployed,setIsEmployed]=useState(false);
  
  const updateName = () => {
    setName("sandeep");
  };

  const updateAge=()=>{
    setAge(age+2);
  }
  const updateEmploy=()=>{
     setIsEmployed(!isEmployed)
  }

  return (
    <div>
      <p>name: {name}</p>
      <button onClick={updateName}>click me</button>

      <p>age :{age}</p>
      <button onClick={updateAge}>click me</button>

      <p>isEmployed :{isEmployed ? "YES":"NO"}</p>
      <button onClick={updateEmploy}>click me</button>
    </div>
    
  );
}

export default MyComponent;
