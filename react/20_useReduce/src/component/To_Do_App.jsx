import React, { useReducer, useState } from 'react';

const initialState = {
  todos: []
};

const todoReducer = (state, action) => {
  switch (action.type) {
    case 'add':
      return {
        todos: [...state.todos, { id: Date.now(), text: action.payload, completed: false }]
      };
    case 'toggle':
      return {
        todos: state.todos.map(todo =>
          todo.id === action.payload ? { ...todo, completed: !todo.completed } : todo
        )
      };
    case 'remove':
      return {
        todos: state.todos.filter(todo => todo.id !== action.payload)
      };
    case 'edit':
      return {
        todos: state.todos.map(todo =>
          todo.id === action.payload.id ? { ...todo, text: action.payload.text } : todo
        )
      };
    default:
      return state;
  }
};

function TodoApp() {
  const [state, dispatch] = useReducer(todoReducer, initialState);
  const [inputValue, setInputValue] = useState('');
  const [editId, setEditId] = useState(null);

  const handleAddTodo = () => {
    if (inputValue.trim() !== '') {
      dispatch({ type: 'add', payload: inputValue });
      setInputValue('');
    }
  };

  const handleToggleTodo = id => {
    dispatch({ type: 'toggle', payload: id });
  };

  const handleRemoveTodo = id => {
    dispatch({ type: 'remove', payload: id });
  };

  const handleEditTodo = (id, newText) => {
    dispatch({ type: 'edit', payload: { id, text: newText } });
    setEditId(null);
  };

  return (
    <div className="todo-app">
      <h1>To-Do App</h1>
      <input
        type="text"
        placeholder="Enter your to-do"
        value={inputValue}
        onChange={e => setInputValue(e.target.value)}
      />
      <button onClick={handleAddTodo}>Add</button>
      <ul>
        {state.todos.map(todo => (
          <li key={todo.id} className={todo.completed ? 'completed' : ''}>
            {editId === todo.id ? (
              <input
                type="text"
                defaultValue={todo.text}
                autoFocus
                onBlur={e => handleEditTodo(todo.id, e.target.value)}
                onKeyDown={e => {
                  if (e.key === 'Enter') {
                    handleEditTodo(todo.id, e.target.value);
                  }
                }}
              />
            ) : (
              <>
                <span onDoubleClick={() => setEditId(todo.id)}>{todo.text}</span>
              </>
            )}
            <button onClick={() => handleRemoveTodo(todo.id)}> X </button>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default TodoApp;
