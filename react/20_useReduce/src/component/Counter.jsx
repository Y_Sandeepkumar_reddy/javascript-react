import React, { useReducer } from 'react';

const initialState = { count: 0 };

const reducer = (state, action) => {
  switch (action.type) {
    case "increment":
      return { count: state.count + 1 };
    case "decrement":
      return { count: state.count - 1 };
    case "reset":
      return { count: 0 }; // Resetting count to 0
    default:
      return state;
  }
};

function Counter() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <div className='main-div'>
      <div className='inner-div'>
        <h1>Count: {state.count}</h1> {/* Display count value */}
        <div className='container'>
          <button className='add-button' onClick={() => dispatch({ type: 'increment' })}>
            ADD
          </button>
          <button className='sub-button' onClick={() => dispatch({ type: 'decrement' })}>
            SUB
          </button>
          <button className='reset-button' onClick={() => dispatch({ type: 'reset' })}>
            RESET
          </button>
        </div>
      </div>
    </div>
  );
}

export default Counter;
