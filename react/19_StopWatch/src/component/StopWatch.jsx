import React, { useEffect, useRef, useState } from 'react'

function StopWatch() {
  const [isRunning,setIsRunning]=useState(false);
  const [elapsedTime,setElapsedTime]=useState(0);
  const intervalRef=useRef(null);
  const startTimeRef=useRef(0);
  useEffect(()=>{
    if (isRunning) {
       intervalRef.current=setInterval(()=>{
         setElapsedTime(Date.now()-startTimeRef.current);
       },10) 
    }
    return ()=>{
        clearInterval(intervalRef.current)
    }

  },[isRunning]);

  const Start=()=>{
    setIsRunning(true);
    startTimeRef.current=Date.now()-elapsedTime;
  };
  const Stop=()=>{
    setIsRunning(false)
  };
  const Reset=()=>{
    setElapsedTime(0);
    setIsRunning(false)
    
  }
  const formatTime=()=>{
    let h=Math.floor(elapsedTime/(1000*60*60));
    let m=Math.floor(elapsedTime/(1000*60)%60);
    let s=Math.floor((elapsedTime/(1000)%60));
    let ms=Math.floor(Math.floor(elapsedTime%1000)/10);


    h=String(h).padStart(2,"0");
    m=String(m).padStart(2,"0");
    s=String(s).padStart(2,"0")
    ms=String(ms).padStart(2,"0")

    return `${h} : ${m} :${s} : ${ms}`
  }

  return (
    <>
    <div className='stopwatch'>
        <div className='display'>{formatTime()}</div>
        <div className='controls'>
            <button onClick={Start} className='start-button'>
                Start
            </button >
            <button onClick={Stop} className='stop-button'> 
                Stop
            </button>
            <button onClick={Reset} className='reset-button'>
                Reset
            </button>
        </div>
    </div>
    
    </>
  )
}

export default StopWatch