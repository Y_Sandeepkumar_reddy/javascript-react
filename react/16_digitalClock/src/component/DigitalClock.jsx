import React,{useState,useEffect} from 'react'

function DigitalClock() {
    const [time, setTime]=useState(new Date());

    useEffect(()=>{
       const intervelID=setInterval(()=>{
           setTime(new Date());
       },1000)

       return ()=>{
        clearInterval(intervelID)
       }
    },[])

    const formatTime=()=>{
        let hours=time.getHours();
        let minutes=time.getMinutes();
        let  seconds=time.getSeconds();
        const meridiun=hours >=12?"PM":"AM";
        hours=hours % 12 ||12;

        return `${setPad(hours)}:${setPad(minutes)}:${setPad(seconds)} ${ meridiun}`
    }
    const setPad=(number)=>{
        return (number < 10 ? 0 : "")+number;
    }
    
  return (
    <div className='clock-container'> 
          <div className='clock'>
             <span>{formatTime()}</span>
          </div>
    </div>
  )
}

export default DigitalClock