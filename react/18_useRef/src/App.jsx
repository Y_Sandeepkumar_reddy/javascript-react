//!       useState() == Re-render the component when the state value changes


//!       useRef()  ==  " use Reference " Does not cause re-renders when its value changes.
//!                        When you want a component to " render " some information.
//!                        But you don't want that information to trigger new renders .

//?       1. Accessing / Interacting with DOM element
//?       2. Handling Focus , Animation, and Transition .


import MyComponent from "./component/MyComponent"
function App() {

  return (
    <>
    <MyComponent/>
    </>
  )
}

export default App
