let a= typeof(8);
console.log(a);

let b= typeof("7");
console.log(b);

let c= typeof(true);
console.log(c);

let d= typeof(NaN);
console.log(d);

let e= typeof(false);
console.log(e);

let f= typeof(Infinity);
console.log(f);

let g= typeof(undefined);
console.log(g);

let h= typeof(31.31);
console.log(h);

let i= typeof(undefined-undefined);
console.log(i);

let j= typeof(Infinity-Infinity);
console.log(j);

let k= typeof(31.31-Infinity);
console.log(k);

let l= typeof(Infinity-35);      // infinity
console.log(l);

let m= typeof(NaN-NaN);        //  Nan
console.log(m);
