// ARRAY

// an array is a special type of object,it contaion the list of ordered  values, and it contain  key value pairs.
// [ ]  sqare brackets are used to create array

let a=[];   // empty array

let array=[21,23,34,56,78];   // array of numbers

let fruits=["orange","mango", "banana","greaps","pulpy","sapota"];

let mixed=[24,"orange",29,"banana","sapota","mango"];




console.log(fruits)
console.log(typeof(fruits))

console.log(fruits[0])    
console.log(mixed[4])


fruits[2]="hi";            // updating in the place of banana
console.log(fruits)

var len=fruits.length
fruits[len]="banana"      //  adding in the last  by finding  length of the 
console.log(len)
console.log(fruits) 


console.log(mixed[2],fruits[3]);      // accessing the values from array     // 


console.log(delete fruits[2])
console.log(fruits)                   //  deletingb specific item based on index value



for(let i=0; i<=fruits.length;i++)
{
    console.log(fruits[i])
}