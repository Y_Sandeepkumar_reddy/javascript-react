// Return Statement



// Return is a special  statement that is used to return some value from a function , and  as soon as a function hits the return the function execution stops  at the same place.

function fullName(frist,last) {
    return `${frist}${last}`;
}
console.log(fullName("arya"," stark"))
console.log(fullName);


// return is only used in functions