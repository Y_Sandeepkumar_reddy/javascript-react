let data = [
    {
        id: 1,
        name: 'John Doe',
        hobbies: ['Reading', 'Swimming', 'Coding']
    },
    {
        id: 2,
        name: 'Jane Smith',
        hobbies: ['Painting', 'Dancing', 'Traveling']
    },
    {
        id: 3,
        name: 'Michael Johnson',
        hobbies: ['Cooking', 'Hiking', 'Photography']
    }
];

// console.log(data);
// console.log(" ")
// console.log(" ")
// console.log("inner data")


console.log(data[0].hobbies);
console.log(data[0].hobbies[1]);
console.log(typeof(data[1].hobbies[2]))
console.log(Number(data[1].hobbies[2]))
