//! EVENTS

// function buttonclicked() {
//     console.log("button clicked ");
// }
//  it apperas on the console in browser and

// var button = document.getElementById("button")
// button.onclick=()=>{
//     let a =prompt("enter your age");
//     if (a<18) {
//         let h1=document.createElement("h1");
//         h1.textContent="you are not eligible for vote";
//         alert(h1.textContent);
//     }
// }

// ?  next one

// var button = document.getElementById("button").addEventListener("click" , buttonclicked);

// function buttonclicked() {
//     document.getElementById("header-title").textContent="I LOVE YOU MOUNIKA ❤️";
//     document.querySelector("#main").style.backgroundColor="#f4f4f4"
// }

//?  we can do optimize

// var button = document.getElementById("button").addEventListener("click" , ()=>{
//     document.getElementById("header-title").textContent="I LOVE YOU MOUNIKA ❤️";
//     document.querySelector("#main").style.backgroundColor="#f4f4f4"
// });

//? THARD ONE

// var button = document.getElementById("button").addEventListener("click" , (e)=>{
//   console.dir(e);            // it shows in object form
// console.log(e.target);          // it shows actual element we clicked
// console.log(e.target.id);
// console.log(e.target.className);
// console.log(e.target.classList);

// var output=document.getElementById('output');
// output.innerHTML='<h3>'+e.target.id+'</h3>' ;

// console.log(e.type);
// });

//? FORTH ONE
// var button = document.getElementById("button").addEventListener("click" , (e)=>{

//     console.log(e.clientX);   // from total screen
//     console.log(e.clientY);
//     console.log(e.offsetX);     // on button
//     console.log(e.offsetY);
// });

//? FIFTH ONE
// var button = document
//   .getElementById("button")
//   .addEventListener("click", (e) => {
//     console.log(e.altKey);
//     console.log(e.ctrlKey);
//     console.log(e.shiftKey);
//   });
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
//! mouse actions

// var button = document.getElementById("button");
// button.addEventListener("click" , runEvent);
// button.addEventListener("dblclick" , runEvent);
// button.addEventListener("mousedown" , runEvent);     // when we pressed the button instantly it give output
// button.addEventListener("mouseup" , runEvent);          // when pressed then relise then it prints

// function runEvent(e) {
//     console.log("EVENT TYPE :"+e.type);
// }

//!  MOUSE EVENTS
// var button = document.getElementById("button");
// var box=document.getElementById("box");

// box.addEventListener('mouseenter',runEvent);     // mouse enter work only when the curser enter on the div
// box.addEventListener('mouseleave',runEvent);    //  mouse leave works when curser leaves the div
//? frist comment mouse leave and mouse out then  abserve the thing

// box.addEventListener('mouseover',runEvent);        // mouse over works when curser on the div and element
// box.addEventListener('mouseout',runEvent);         // mouse out works every time curser leaves element and div

// function runEvent(e){
//     console.log("EVENT TYPE :"+e.type);                // when we place cursor on the box it prints
// }

//! MOUSE MOVES

// box.addEventListener('mousemove',runEvent);

// function runEvent(e){
//    console.log("EVENT LIOST : "+e.type);
//    let output=document.getElementById("output");
//    output.innerHTML='<h3>MouseX: '+e.offsetX+'</h3><h3>MouseY : '+e.offsetY+'</h3>';

//? changing color based on axes:

// box.addEventListener("mousemove", runEvent);
// function runEvent(e) {
//   document.body.style.backgroundColor =
//     "rgb(" + e.offsetX + "," + e.offsetY + ",240)";
//   box.style.backgroundColor = "rgb(" + e.offsetX + "," + e.offsetY + ",90)";
// }

//! next topic    INPUT  countdown and catch

// var iteminput = document.querySelector('input[type="text"]');
// var form = document.querySelector("form");

// iteminput.addEventListener("keydown", runEvent);
// iteminput.addEventListener("keyup", runEvent);
// iteminput.addEventListener("key", runEvent);
// iteminput.addEventListener("focus", runEvent);
// iteminput.addEventListener("blur", runEvent);
// iteminput.addEventListener("cut", runEvent);
// iteminput.addEventListener("past", runEvent);
// iteminput.addEventListener("input", runEvent);
                                                            // when we press any key in the  input field it displays the key down in the console
// function runEvent(e) {  
//   console.log("EVENT TYPE :" + e.type);
 //   console.log(e.target.value);

//   document.getElementById("output").innerHTML='<h3>'+e.target.value+'</h3>'

// }
//? catching the  data

// var select = document.querySelector("select");
// select.addEventListener("change",runEvent)

// function runEvent(e) {  
//     console.log("EVENT TYPE :" + e.type);
//     console.log(e.target.value);
//     document.getElementById("output").innerHTML='<h3>'+e.target.value+'</h3>'    
// }





//? form

var form = document.querySelector("form");
form.addEventListener("submit",(e)=>{
    e.preventDefault(); 
      console.log("EVENT TYPE :"+e.type);
});

