// EXAMINE THE DOCUMENT OBJECT
// console.dir(document.domain);
// console.dir(document.URL);
// console.dir(document.title);
// console.dir(document.title="sandeep");
// console.dir(document.doctype);
// console.dir(document.domain);
// console.log(document.head);
// console.log(document.body);
// console.log(document.styleSheets);
// console.log(document.all);      // not working,  have  to chick this
// console.log(document.all[11]);
// console.log(document.all[11].textContent="smr")  // we can also change the conte t but is is not recomended way
// console.log(document.forms);
// console.log(document.links);

// //? examine the document

/// console.dir(document)

// console.log(document.domain);

// console.log(document.URL);
// console.log(document.title);

// //? we can also change things      i am changing title

// console.log(document.title=123);   // title means file name

// console.log(document.doctype);
// console.log(document.head);
// console.log(document.body);
// console.log(document.all[13])
// document.all[10].textContent="sandeep"    //   but it is not the correct way to do that

// console.log(document.forms[0]);    // it displays the forms that we are used in out program
// console.log(document.links);
// console.log(document.links[0]);
// console.log(document.images);
// console.log(document.images);   // currently in html page there is no images

//!   selectors

//! get element by id
// console.log(document.getElementById("tittle"));
var a = document.getElementById("header-title");
// console.log(a);
// console.log(a.textContent );                // it displayed the text content   even the style display :none
// console.log(a.innerText);
a.textContent = "SANDEEP";
// console.log(a.innerText="mouni");
// console.log(a.textContent="mounika");

// console.log(a.innerHTML='<h2>how are you</h2>');   // in code it does not change or appear but  in browser console inside  h1 tag it appears

//? now changing the style

// a.style.borderBottom = "solid 3px #000";
// a.style.color = "red";
// a.style.backgroundColor = "aqua";

// now i am trying to add border for  header

// b = document.getElementById("main-header");
// console.log(b);
// b.style.borderBottom = "solid 3px gold";
// b.style.borderRight = "solid 3px gold";

//!  get elemenyt by  class name
// var items= document.getElementsByClassName("list-group-item")
// console.log(items);
// console.log(items[1]);
// items[2].textContent="mounika";
// items[2].style.fontWeight="bold";
// items[2].style.backgroundColor="red";

//? gives error
// items.style.backgroundColor="aqua";

// for(let i=0; i<items.length;i++){
//     items[i].style.backgroundColor="aqua";   //    we arer iterating for all  element for add colors
// }
// items[2].style.backgroundColor="red";

//!   GET ELEMENT BY TAG NAME

// var li = document.getElementsByTagName('li')
// console.log(li);

// for(let i of li){
//     i.style.backgroundColor="aqua";
// }

// li[1].style.fontWeight="bold"
// li[1].style.backgroundColor="orange";
// li[1].textContent="i love mounika";
// li[1].style.color="red";

//!     QUERY SELECTORS

var header = document.querySelector("#main-header");
// console.log(header);
header.textContent = "SANDEEP";
header.style.fontSize = "40px";

//?    The querySelector() method returns the first element that matches a CSS selector.
//?    To return all matches (not only the first), use the querySelectorAll() instead.
//?    Both querySelector() and querySelectorAll() throw a SYNTAX_ERR exception if the selector(s) is invalid.

// var input =document.querySelector("input")
// input.value="hello world";

// var submit= document.querySelector('input[type=submit]');
// submit.value="send";

// var  item=document.querySelector(".list-group-item");
// item.style.color="red";

// var  item=document.querySelector(".list-group-item:nth-child(2)");
// item.style.color="blue";

// var  item=document.querySelector(".list-group-item:nth-child(3");
// item.style.color="aqua";

// var  lastItem=document.querySelector(".list-group:last-child");
// lastItem.style.color = "red";

//! QUERY SELECTOR ALL

var titles = document.querySelectorAll(".title");
// console.log(titles);
// titles[0].textContent = "SANDEEP";
// titles[1].textContent = "MOUNIKA";

// var even = document.querySelectorAll("li:nth-child(even)");
// var odd = document.querySelectorAll("li:nth-child(odd)");

// for (let i = 0; i < even.length; i++) {
//   even[i].style.backgroundColor = "#f4f4f4";
//   odd[i].style.backgroundColor = "#f4f4";
// }


// TRAVERSING THE DOM

// var itemlist = document.querySelector("#items");
// console.log(itemlist.parentNode);

// itemlist.parentNode.style.backgroundColor = "#f4f4";
// console.log(itemlist.parentNode.parentNode);
// console.log(itemlist.parentNode.parentNode.parentNode);

//? parent element
// var itemlist = document.querySelector("#items");
// console.log(itemlist.parentElement);
//  itemlist.parentElement.style.backgroundColor = "#f4f4";
// console.log(itemlist.parentElement.parentElement);
// console.log(itemlist.parentElement.parentElement.parentElement);

//? child nodes
// var itemlist = document.querySelector("#items");
// console.log(itemlist.childNodes);

//?  child element
var itemlist = document.querySelector("#items");
// console.log(itemlist.children);

// console.log((itemlist.children[1].style.backgroundColor = "red"));

//?  frist chils
// console.log(itemlist.firstChild);
//? frist Element chiild
// console.log(itemlist.firstElementChild);
// itemlist.firstElementChild.textContent="Mounika"
// itemlist.firstElementChild.nextElementSibling.textContent="Sandeep"

//? last child
// console.log(itemlist.lastChild);

//? last element child
// console.log(itemlist.lastElementChild);

//? next sibling
// console.log(itemlist.nextElementSibling);

//? next element sibling

// console.log(itemlist.nextElementSibling);

//? previous  sibling

// console.log(itemlist.previousSibling);

//? previous element sibling
// console.log(itemlist.previousElementSibling);
// itemlist.previousElementSibling.style.color="red"