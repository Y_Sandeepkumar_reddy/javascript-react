// console.log(document.all);
// let ul = document.querySelector("ul");
// console.log(ul);

// console.log(ul.parentElement);

// console.log(ul.firstElementChild);
// console.log(ul.lastElementChild);

// console.log(ul.previousElementSibling);
// console.log(ul.nextElementSibling);

//? DOM manupulation

// var h1=document.getElementsByTagName("h1")
// console.log(h1);
// console.log(h1.length);

// var h1 = document.querySelector("h1");
// console.log(document.querySelectorAll("li"));
// console.log(h1.innerText);
// console.log(h1.textContent);
// console.log(h1.innerHTML);
// console.log((h1.innerText = "sandeep"));
// console.log((h1.textContent = "kumar"));

// let header = document.querySelector("h1");
// console.log((header.innerText = "hello<h1>sandeep</h1>"));
// console.log((header.innerHTML = "hello<h1>sandeep<h1>"));
// console.log(header.tagName);

// let div = document.querySelector(".info");
// console.log(div);
// console.log(div.children);
// console.log(div.children[1].children);

//? Style

// console.log((div.style.color = "tomato"));
// console.log((div.style.backgroundColor = "aqua"));

//? value  href and src

// let img = document.querySelector("img");
// console.log(img.src="http://127.0.0.1:5500/v-2/one.html");

//! input elements

// var textInput = document.querySelector('input[type="text"]');
// console.log(textInput);
// console.dir(textInput);
// console.log((textInput.value = "hello world"));        //   chick before comment and after comment

// let checkbox = document.querySelector('input[type="checkbox"]');

// let range = document.querySelector('input[type="range"]');
// console.log((range.value = "95%"));

//! methods
//?  1.setAttribute 2. getAttribute    3. hasAttribute  4. createElement  5. append  6. prepend  7. remove   8. classList

//? setAttribute ----------------------------------------------------------------------------------------------------------------------------

let setAtt = document.querySelector(".info");
setAtt.setAttribute("id", "div1");
setAtt.setAttribute("class", "div-main");
console.log(setAtt); // in console we can check that id and class attributes are added

//? getAttribute -------------------------------------------------------------------------------------------------------------------------------------------------

let li = document.querySelector("li");
console.log(li.getAttribute("class"));

//? has Attribute --------------------------------------------------------------------------------------------------------------------------------------------------
console.log(li.hasAttribute("id"));
console.log(li.hasAttribute("class"));

//? createElement  -------------------------------------------------------------------------------------------------------------------------------------------------
var list = document.createElement("li");
console.log(typeof document.createElement("li"));
list.innerText = "query selectors";
console.log((list.innerText = "query selectors"));
console.log(list);
list.className = "list4";
list.setAttribute("id", "good");

//? append --------------------------------------------------------------------------------------------------------------------------------------------------------
let ul = document.querySelector("ul");

ul.append(list);
console.log(ul);

var list2 = document.createElement("li");
list2.textContent = "method-2";

let mdiv = document.querySelector("body > div");
let tdiv = mdiv.querySelector("div > div > ul");
console.log(tdiv);
tdiv.insertBefore(list2, tdiv.querySelector("p"));

let list3 = document.createElement("li");
list3.textContent = "method-3";
let list4 = document.createElement("li");
list4.textContent = "method-3";

console.log(ul.append(list3, list4));

//? prepend-------------------------------------------------------------------------------------------------------------------------------------------------------

console.log(ul.prepend(list4));
let a = document.createElement("a");
a.textContent = "demo data";
a.setAttribute("id", "demo");
console.log(ul.prepend(a));
//? remove-----------------------------------------------------------------------------------------------------------------------------------------------------------

let rm = document.querySelector("a");
console.log(rm);
rm.remove();

//? class list---------------------------------------------------------------------------------------------------------------------------------------------------
var p=document.querySelector("p");
p.className="para";
console.log(p);             // adding   class name to the  para .
