 
//?   The sort() sorts the elements of an array.
//?   The sort() overwrites the original array.
//?   The sort() sorts the elements as strings in alphabetical and ascending order.
/*
Sort Compare Function

--> Sorting alphabetically works well for strings ("Apple" comes before "Banana").
--> But, sorting numbers can produce incorrect results.
--> "25" is bigger than "100", because "2" is bigger than "1".
--> You can fix this by providing a "compare function" (See examples below)

*/

console.log("1:");
console.log(
  " --------------------------------------------------------------------- "
);
let numbers = [23, 45, 75, 96, 3, 6, 36, 85];

numbers.sort(function num(a, b) {
  //   < 0 ... a comes frist
  //   = 0  .. no change
  //   > 0  ..  b comes secomnd

  return a - b;
});

console.log(numbers);

console.log(
  "-------------------------------------------------------------------------"
);
console.log("2: ");
console.log(" ");

let products = [
  {
    nam: "sandeep",
    age: 24,
  },
  {
    nam: "sandheep",
    age: 23,
  }, // we are not giving any a and b values
  {
    nam: "reddy",
    age: 30,
  },
];
products.sort((a, b) => {
  return a.age - b.age;
});
console.log(products);

console.log(
  "  assignment problem 1: -------------------------------------------------------------------------"
);
// problem 1
const z = [5, 3, 8, 1, 9, 2, 7];

z.sort((a, b) => {
  return a - b;
});
console.log(z);

// Sort an array of strings in descending order.
console.log(
  "  assignment problem 2: -------------------------------------------------------------------------"
);
console.log(" ");
const words = ["banana", "apple", "orange", "grapes", "kiwi"];

words.sort();
words.reverse();
console.log(words);

// Sort an array of objects based on a specific property in ascending order.
console.log(
  "  assignment problem 3: -------------------------------------------------------------------------"
);
console.log(" ");
const students = [
  { name: "John", age: 25 },
  { name: "Alice", age: 30 },
  { name: "Bob", age: 20 },
  { name: "Emma", age: 22 },
];

students.sort((a, b) => {
  return a.age - b.age;
});
console.log(students);

// Sort an array of dates in ascending order.
console.log(
  "  assignment problem 4: -------------------------------------------------------------------------"
);
console.log(" ");
const dates = [
  new Date("2023-03-15"),
  new Date("2022-10-01"),
  new Date("2024-01-30"),
  new Date("2022-07-20"),
];
dates.sort((a, b) => a - b);
console.log(dates);

// Sort an array of objects containing dates based on the date property in descending order.
console.log(
  "  assignment problem 5: -------------------------------------------------------------------------"
);
console.log(" ");
const events = [
  { name: "Event A", date: new Date("2023-05-10") },
  { name: "Event B", date: new Date("2022-12-25") },
  { name: "Event C", date: new Date("2024-02-15") },
  { name: "Event D", date: new Date("2022-08-05") },
];
events.sort((a, b) => b.date.getTime() - a.date.getTime());

console.log(events);

// Sort an array of strings by their length in ascending order.
console.log(
  "  assignment problem 6: -------------------------------------------------------------------------"
);
console.log(" ");
const wordsLength = ["banana", "apple", "orange", "grapes", "kiwi"];
wordsLength.sort((a, b) => a.length - b.length);
console.log(wordsLength);

//  Sort an array of objects based on multiple criteria (e.g., sort by age first and then by name).
console.log(
  "  assignment problem 7: -------------------------------------------------------------------------"
);
console.log(" ");

const people = [
  { name: "John", age: 25 },
  { name: "Alice", age: 30 },
  { name: "Bob", age: 20 },
  { name: "Emma", age: 22 },
  { name: "Jane", age: 25 },
  { name: "David", age: 30 },
];
people.sort((a, b) => a.age - b.age);
console.log(people);

people.sort((a, b) => {
  return a.name.localeCompare(b.name);
}); // if an array of object contain more than 1 values, in that we are trying to sort  strings we can use lacal comapare /
console.log(people);

// Given an array of dates, find the earliest date.
console.log(
  "  assignment problem 8: -------------------------------------------------------------------------"
);
console.log(" ");

const zc = [
  new Date("2023-03-15"),
  new Date("2022-10-01"),
  new Date("2024-01-30"),
  new Date("2022-07-20"),
];
zc.sort((a, b) => a - b);
console.log(zc);
const ed = zc[0];
console.log(`earlist day is ${ed}`);

// Given an array of dates, find the latest date.
console.log(
  "  assignment problem 9: -------------------------------------------------------------------------"
);
console.log(" ");

const zb = [
  new Date("2023-03-15"),
  new Date("2022-10-01"),
  new Date("2024-01-30"),
  new Date("2022-07-20"),
];
zb.sort((a, b) => {
  return a - b;
});
let r = zb.reverse();
console.log(r[0]);

// todo   Sort an array of objects containing dates based on the year property in ascending order.

console.log(
  "  assignment problem 10: -------------------------------------------------------------------------"
);
console.log(" ");

const za = [
  { name: "Event A", date: new Date("2023-05-10") },
  { name: "Event B", date: new Date("2022-12-25") },
  { name: "Event C", date: new Date("2024-02-15") },
  { name: "Event D", date: new Date("2022-08-05") },
];

za.sort((a, b) => {
  return a.date.getFullYear() - b.date.getFullYear();
});
console.log(za);
