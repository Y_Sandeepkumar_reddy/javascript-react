// Array Interview Programs (Level 3):

//     Implement an algorithm to find all subsets of a given array (powerset).
//     Find the longest common subsequence between two arrays of integers.
//     Given an array of integers, find the maximum sum of a subarray with at least k elements.
//     Calculate the product of all elements except the current element in an array without using division.
//     Implement an algorithm to sort an array of integers in O(n) time and space complexity.
//     Find the median of two sorted arrays of equal length in O(log n) time complexity.
//     Determine if there exists a subarray with a sum equal to zero in an unsorted array.
//     Given an array of integers, rearrange the array in alternating positive and negative numbers.
//     Implement an algorithm to find the longest palindromic substring in a given string.
//     Calculate the maximum area of a histogram represented by an array of heights.