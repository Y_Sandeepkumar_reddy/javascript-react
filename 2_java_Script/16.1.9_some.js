
//-->//?    The some() method checks if any array elements pass a test (provided as a callback function).

//-->//?   The some() method executes the callback function once for each array element.

//-->//?    The some() method returns true (and stops) if the function returns true for one of the array elements.

//-->//?    The some() method returns false if the function returns false for all of the array elements.

//-->//?   The some() method does not execute the function for empty array elements.

//--> //?   The some() method does not change the original array.

let array = [1, 3, 5, 7];

let a = array.some((n) => {
  return n > 5;
});
console.log(a);

console.log("->2:  ---------------------------------------------------");

let b = array.some(greater);

function greater(n) {
  return n % 2 == 0;
}
console.log(b);

console.log("-> 3:  -------------------------------------------------");

const c = [
  {
    name: "sandheep",
    age: 24,
  },
  {
    name: "deep",
    age: 26,
  },
  {
    name: "kumar",
    age: 24,
  },
];

let d = c.some(age);
function age(a) {
  return a.age >= 20;
}
console.log(d); //?   true
