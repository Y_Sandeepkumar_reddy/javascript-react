//?  The filter() method creates a new array filled with elements that pass a test provided by a function.

//?  The filter() method does not execute the function for empty elements.

//?  The filter() method does not change the original array.

/*
Syntax:
array.filter(function(currentValue, index, arr), thisValue)
*/

//! filter odd numbers from the array
let array=[5, 12, 8, 20, 3, 17, 10, 25, 1, 15, 9, 30, 7, 22, 18, 6, 11, 4, 28, 14];
sortedArray=array.sort((a,b)=>{
    return a-b
})

let result=array.filter(odd)
function odd(arr) {
    return arr%2!==0
}
console.log(result);

//! find even numbers from an array

let r2=array.filter((arr)=>  arr%2==0
)
console.log(r2.reverse());

//! print all element greater than 5 

let r3=array.filter((arr)=>{
    return arr>5
})
console.log(r3);

