//? The join() method returns an array as a string.

//? The join() method does not change the original array.

//? Any separator can be specified. The default is comma (,).



let numbers = [1, 2, 3, 4, 5];
let joinedString = numbers.join(', '); // Join numbers with a comma and space as separator
console.log(joinedString); // Output: "1, 2, 3, 4, 5"



let za=[1,2,3]
let zb=["a","b","c"]
let zc=za.concat(zb)
let zd=zc.join("-")
console.log(zd);



let str="Write a function that takes an array as input and returns a new array with the elements reversed";


let b=(str)=>{
    let z=[]
  let a=str.split(" ")
    for(let i=0;i<a.length;i++)
    {
    let b= a[i].split("")
    b.reverse()
    let c=b.join("")
    z.push(c)
    }
    console.log(z.length)
    return z.join(" ")
  
  
    
}
console.log(b(str))