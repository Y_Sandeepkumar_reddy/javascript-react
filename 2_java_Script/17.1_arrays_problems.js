// Array Questions:


// Find the second largest element in an array.
// Reverse an array without using the built-in reverse method.
// Remove all occurrences of a specific element from an array.
// Find the sum of all positive numbers in an array.
// Merge two arrays into a single array without duplicates.
// Count the number of occurrences of each element in an array and store them in an object.
// Check if an array is sorted in ascending order.
// Return the union of two arrays (containing unique elements from both arrays).
// Rotate an array to the right by a given number of steps.
// Implement a function that shuffles an array randomly.
// Find the missing number in an array of integers from 1 to n.
// Split an array into chunks of a specified size.
// Find the maximum product of two numbers in an array.
// Check if an array contains any duplicate elements.
// Remove the duplicates from a sorted array in-place.
// Implement a function that returns the indices of two numbers in an array that add up to a specific target.
// Sort an array of strings based on their lengths.
// Implement a function to find the intersection of two arrays.
// Find the longest consecutive sequence of numbers in an unsorted array.
// Calculate the difference between the largest and smallest elements in an array.

//     Write a program to find the largest number in an array of integers.
let a = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 169, 17, 18, 19, 99, 20,
  21, 22, 23, 24, 13, 16, 11, 12, 21, 15, 17, 25, 26, 27, 28, 29, 30, 31, 32,
  33, 34, 35, 36, 37, 38, 39, 40,
];
function largestNmber(a) {
  length = a.length;
  let max = a[0];
  for (let i = 1; i < length; i++) {
    if (max < a[i]) {
      max = a[i];
    }
  }
  console.log(max);
}
largestNmber(a);

//     Create a function that returns the sum of all numbers in an array.
let b = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 169, 17, 18, 19, 99, 20,
  21, 22, 23, 24, 13, 16, 11, 12, 21, 15, 17, 25, 26, 27, 28, 29, 30, 31, 32,
  33, 34, 35, 36, 37, 38, 39, 40,
];
function arraySum(a) {
  let sum = 0;
  a.forEach((n) => {
    sum = sum + n;
  });
  console.log(sum);
}
arraySum(b);

//     Implement a program that removes duplicates from an array and returns a new array with unique elements.
function duplicate(a) {
  let length = a.length;
  let array = [];
  let repeated = [];
  for (let i = 0; i < length; i++) {
    let duplicate = false;
    for (let j = i + 1; j < length; j++) {
      if (a[i] == a[j]) {
        repeated.push(a[j]);
        duplicate = true;
      }
    }
    if (duplicate == false) {
      array.push(a[i]);
    }
  }
  console.log(repeated);
  console.log(array);
}
duplicate(a);

console.log("New Problem");

function sum() {
  let a = [1, 2, 3, 6, 7];
  let b = [6, 7, 8, 9, 10];
  let array = [...a, ...b];
  console.log(array);

  let newArray = [];
  let duplicate = [];
  for (let i = 0; i < array.length; i++) {
    let isDuplicate = false;
    for (let j = i + 1; j < array.length; j++) {
      if (array[i] == array[j]) {
        duplicate.push(array[i]);
        isDuplicate = true;
      }
    }
    if (!isDuplicate == true) {
      newArray.push(array[i]);
    }
  }
  console.log(newArray);
  console.log(duplicate);
}
sum();

//     Implement a program that removes duplicates from an array and returns a new array with unique elements.
function removeDuplicates() {
  let a = [1, 3, 1, 4, 6, 3, 8, 9];
  let newArray = [];
  let duplicates = [];
  a.filter((n) => {
    if (newArray[n]) {
      duplicates.push(n);
    } else {
      newArray.push(n);
    }
  });
  console.log(newArray);
  console.log(duplicates);
}
removeDuplicates();

//     Write a function to check if two arrays are equal (having the same elements in the same order).
let arr1 = [1, 2, 3];
let arr2 = [1, 2, 3];
function arrayComparision(arr1, arr2) {
  if (arr1.length === arr2.length) {
    let equal = true;
    for (let i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) {
        equal = false;
      }
    }
    if (equal) {
      console.log("true");
    } else {
      console.log("falswe");
    }
  } else {
    console.log("both are not equal");
  }
}
arrayComparision(arr1, arr2);

function arrayCompe(arr1, arr2) {
  const isEqual =
    arr1.length === arr2.length &&
    arr1.every((element, index) => element === arr2[index]);

  if (isEqual) {
    console.log("Arrays are equal.");
  } else {
    console.log("Arrays are not equal.");
  }
}
arrayCompe(arr1, arr2);

//     Develop a program that finds the second smallest number in an array of integers.

function second_smallest() {
  const sorted_Array = a.sort((a, b) => a - b);
  length = sorted_Array.length;

  if (length >= 2) {
    console.log(sorted_Array[1]);
  } else {
    console.log("array should have atleast two elements");
  }
}
second_smallest();

//     Create a function that sorts an array of strings in alphabetical order.
function sort() {
  let string = ["sandeep", "kumar", "reddy"];
  console.log(string.sort());
}
sort();

//     Write a program to calculate the average of numbers in an array.

function avg(a, l) {
  sum = 0;
  a.forEach((n) => {
    sum += n;
  });
  let avg = sum / l;
  console.log(avg.toFixed(3));
}
avg(a, a.length);

//     Implement a function to reverse an array without using the built-in reverse() method.
       console.log(a.reverse());

//     Create a program that finds the intersection of two arrays (common elements between them).
function common() {
    let arr4=[1,2,3,4,5]
let arr5=[6,7,8,5,9,9]

repeted=[]
arr4.forEach((x)=>{
    if(arr5.includes(x))
    {
        repeted.push(x)
    }
    
})
console.log(repeted);
}
common()

//     Write a function to rotate an array by a specified number of positions.
// const inputArray = [1, 2, 3, 4, 5];
// const positionsToRotate = 2;

// Original Array: [1, 2, 3, 4, 5]
// Rotated Array: [4, 5, 1, 2, 3]

let array = [1, 2, 3, 4, 5];
let position = 3;

function rotatedArray(arr, pos) {
  let rotatedArray = [...arr.slice(pos), ...arr.slice(0, pos)];
  console.log("Original Array:", arr);
  console.log("Rotated Array:", rotatedArray);
}

rotatedArray(array, position);
