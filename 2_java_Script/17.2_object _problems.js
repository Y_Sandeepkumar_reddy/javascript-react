// Object Questions:

//     Implement a program that checks if a given object has a specific property.
//     Write a function to merge two objects into one, combining their key-value pairs.
//     Create a program to count the number of occurrences of each element in an array and store the counts in an object.
//     Implement a function that converts an object into an array of key-value pairs.
//     Write a program to check if two objects have the same key-value pairs.
//     Create a function to deep clone an object (including nested objects and arrays).
//     Write a program that calculates the total value of properties in an object (assuming numeric values).
//     Implement a function to filter an array of objects based on a specific property value.
//     Create a program that checks if an object is empty (has no properties).
//     Write a function to sort an array of objects based on a specific property (e.g., age, name, etc.).

// Check if an object has a specific property.
// Merge two objects, combining their key-value pairs.
// Find the number of keys in an object.
// Convert an object to an array of key-value pairs.
// Check if two objects have the same key-value pairs.
// Deep clone an object (including nested objects and arrays).
// Calculate the total value of numeric properties in an object.
// Filter an array of objects based on a specific property value.
// Implement a function to flatten a nested object into a single-level object.
// Convert an array of objects into an object with key-value pairs.
// Find the average value of numeric properties in an object.
// Check if an object is empty (has no properties).
// Sort an array of objects based on a specific property (e.g., age, name, etc.).
// Calculate the total number of values in an object (including nested objects and arrays).
// Remove a specific property from an object.

// Object Interview Programs (Level 2):

//     Calculate the total value of properties in an object recursively (including nested objects and arrays).
//     Implement a function to find the deepest nested key in an object.
//     Flatten an object with nested keys into a flat object with dot-separated keys.
//     Given an array of objects, find the object with the maximum value of a specific property.
//     Implement a function to filter an array of objects based on multiple criteria (property values).
//     Calculate the sum of values for each unique key in an array of objects.
//     Find the frequency of occurrence of each word in a given string and store it in an object.
//     Convert a CSV string (comma-separated values) into an array of objects with key-value pairs.
//     Check if two objects are deeply equal (have the same structure and values, including nested objects and arrays).
//     Group an array of objects by a specified property and create an object with grouped data.

// Object Interview Programs (Level 3):

//     Flatten an object with nested keys into a flat object with camel-case keys.
//     Calculate the intersection of multiple arrays of objects based on a common property.
//     Implement an algorithm to deep compare two objects with circular references.
//     Check if a given object is a valid JSON object (no circular references).
//     Given an array of objects, find the object with the minimum value of a specific property.
//     Calculate the difference between two arrays of objects based on a common property.
//     Group an array of objects by multiple properties and create a nested object with grouped data.
//     Implement a function to merge an array of objects into a single object with unique keys.
//     Calculate the similarity score between two objects based on their properties and values.
//     Check if an object contains a specific value recursively (including nested objects and arrays).