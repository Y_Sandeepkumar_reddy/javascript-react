// FIND



//? The find() method returns the value of the first element that passes a test.

//? The find() method executes a function for each array element.

//? The find() method returns undefined if no elements are found.

//? The find() method does not execute the function for empty elements.

//? The find() method does not change the original array.

//!   

const { colors, fruits, numbers, data } = require("./0_data");

console.log("1: ");

let a=colors.find(find_item)

function find_item(colors) {
    return   colors==="Navy"
}
console.log(a);

console.log("------------------------------------------");



console.log(" ");
console.log("2:");


let c=colors.find((colors)=>{
      return colors==="Lime"
})
console.log(c);





console.log("------------------------------------------");
console.log("3:");
console.log(" ");


let d=((data)=>{
    let desc={}
    for (const key in data) {
           if(key==='employees')
             {
                data[key].forEach(element => {
                   const z= element.projects.find(project=>project.name ==="Project A")
                    if (z) {
                        desc[z.name]=z.description
                    }
                    
                });
             }
        }
        return desc
    }
)
console.log(d(data));

