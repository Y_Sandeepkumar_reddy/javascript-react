// folder_create()

const { error } = require('console');


function folder_create() {

    const fs=require('fs')
    const path=require('path')



const folderPath=path.join(__dirname,"demo");
fs.mkdir(folderPath,{ recursive: true},(error)=>{
    if (error)
    {
        console.log("error in creating of folder");
    }
    else
    {
        console.log("folder is created");
    }
   
});

}



// file_create();

function file_create() {
    
    const fs=require('fs')
    const path=require('path')
     
    const folderPath=path.join(__dirname,"demo")

    fs.writeFile(path.join(folderPath,"create_folder.txt"),"good",
    (err)=>{
        if (err) throw err ;
        console.log("all is welll");
    });

    }     
    //?  if we have already a file with same name in that folder and  if we try to create with same name   the content in the file is over writen  instead of create an  another file




//!    another  way
// file_create_using_error_handling()

function file_create_using_error_handling(params) {
    const fs = require('fs');
const path = require('path');

const folderPath = path.join(__dirname, "demo");

try {
    fs.writeFileSync(path.join(folderPath, "create_folder.txt"), "god");
    console.log("File created successfully");
} catch (err) {
    console.error("Error creating file:", err);
}
}






// append_file()

function append_file() {
    
   const fs=require('fs')
   const path=require('path')

   const file_path=path.join(__dirname,"demo","create_folder.txt")
     
   fs.appendFile(file_path," how can i help you",(err)=>{
    if(err) throw err;
    console.log("all is well  file is appended");
   })
   
}



// read_file();
function read_file() {
    
    const fs=require('fs')
    const path=require('path')

    file_path=path.join(__dirname,"demo","create_folder.txt")
    fs.readFile(file_path,"utf8" ,
    (err,data)=>{
        if(err) throw err
        console.log(data);
    })
}





// rename_file()

function rename_file() {

    const fs=require('fs')
    const path=require('path')

    file_path=path.join(__dirname,"demo",)
     fs.rename(path.join(file_path,"a.txt"),path.join(file_path,"sandheep.txt"),
     (err)=>{
        if (err) {
            console.log("error accurs");
        }
        else{
            console.log("all is well");
        }
     })
}




// delete_file()
function delete_file() {
   
    const fs=require('fs')
    const path=require('path') 
    file_path=path.join(__dirname,"demo","sandheep.txt")
      
    fs.rm(file_path,
(err)=>{
    if (err) throw err
    console.log("all is well");
})
    
}



//!   create and update  file by user input

// user()
function user() {
    
const readline = require('node:readline');
const fs=require('fs')
const path=require('path')
const { stdin: input, stdout: output } = require('node:process');
const { writeFile, writeFileSync } = require('node:fs');

const rl = readline.createInterface({ input, output });


   rl.question(' what is the file name ?',(FileName)=>{
     rl.question('What  is the content of your file ? ', (content) => {

  rl.close();

        const f_path=path.join(__dirname,"demo")
     fs.writeFile(path.join(f_path,FileName),content,(err)=>{
        if (err) {
            console.error("facing issue in creation of file");
        }
        else{
            console.log("file is created using  readline ");
        }
     });


   });
});

}




//! using try and catch

try_catch()

function try_catch() {
    

    const fs=require('fs')
    const path=require('path')
    const f_path=path.join(__dirname,"demo","h.txt")
    try {
        fs.rm(f_path,(error)=>{
            if(error){
            console.log("error in file deleting");
            }
            else{
                console.log("file deleted successfully");
            }
        })
    } catch (error) {
        console.log("error in file deleting");
    }


}



