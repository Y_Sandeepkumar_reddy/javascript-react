const path=require('path');
const { log } = require('util');

console.log(path.dirname(__filename));    //      /media/hp/Local Disk/AAAA/3_asynchronous-javascript/2_Asyc_Program   --> given directort upto  current folder 

console.log(path.basename(__filename));     //    1_path_module.js  -->  it is given only file name

console.log(path.extname(__filename));       //  .js     --> gives extansion 

console.log(path.parse(__filename));           // it displays root , directory , extension , nmae    and    base

console.log(path.parse(__filename).base);      //    1_path_module.js   -->    gives file name

// concat path


console.log(path.join(__dirname,'test','hello'));    //  /media/hp/Local Disk/AAAA/3_asynchronous-javascript/2_Asyc_Program/test/hello

// delimeter

console.log(path.delimiter);