const readline = require('node:readline');


const { stdin: input, stdout: output } = require('node:process');


const rl = readline.createInterface({ input, output });

  // TODO: Log the answer in a database
     rl.question('What  is the content of your file ? ', (content) => {
         console.log(`hi ${content}`);
  rl.close();

   });


   /*
 
     "stdin" and "stdout" are both standard streams in Node.js representing input and output, respectively. 
       They are commonly used for communication between a Node.js program and its environment, such as a command-line interface or another process.



    -->  We use readline.createInterface() to create a readline interface (rl) that reads input from stdin and prints output to stdout.
    -->  We prompt the user for their name using rl.question(), which reads input from stdin.
    -->  We print a greeting message to stdout using console.log().
    -->  Finally, we close the readline interface with rl.close().

   */







