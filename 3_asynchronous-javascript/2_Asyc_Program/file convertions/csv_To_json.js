 //!   this is used to convert csv to json data

function a() {
    const path=require('path')
const csvFilePath=path.join(__dirname,"address.csv")
const csv=require('csvtojson')
csv()
.fromFile(csvFilePath)                                             // no need of using function  we can remove it
.then((add)=>{
    console.log(add);
   
})

}
a()


 //? ---------------------------------------------------------------------------------------------

 //!  creating the an file and storing  data  into  it 

 
 const path = require('path');
const csv = require('csvtojson');
const fs = require('fs');

const csvFilePath = path.join(__dirname, "address.csv");
const jsonFilePath = path.join(__dirname, "demo");

function convert(csvFilePath, jsonFilePath, name) {
    csv()
    
        .fromFile(csvFilePath)
        .then((data) => {
            try {
                fs.writeFile(path.join(jsonFilePath, name + '.json'), JSON.stringify(data), (error) => {
                    if (error) {
                        console.error("Error occurred:", error);
                    } else {
                        console.log("File converted successfully. JSON file created at:", path.join(jsonFilePath, name + '.json'));
                    }
                });
            } catch (error) {
                console.log("Error converting CSV file into JSON file:", error);
            }
        })
       
}

convert(csvFilePath, jsonFilePath, "address");
