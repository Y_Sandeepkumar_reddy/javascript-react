// javascript is  a synchronous single -thread language , it just do one thing at a time


// one call back contain another call back  like perimid of doom , it give issues in  
//?   Readability: Code becomes hard to read and understand due to deep nesting.
//?     Maintainability: It becomes challenging to modify or extend the codebase because of its complex structure.
//?     Error handling: Error handling becomes more complex, leading to potential bugs or overlooked error cases.
//?     Debugging: Debugging becomes difficult because of the convoluted control flow.

// Function that takes two numbers and a callback function as arguments
function add(a, b, callback) {
    // Perform addition
    const result = a + b;
    // Invoke the callback function with the result
    callback(result);
}

// Callback function to display the result
function displayResult(result) {
    console.log("The result is:", result);
}

// Call the add function with two numbers and the displayResult callback
add(5, 3, displayResult); // Output: The result is: 8




