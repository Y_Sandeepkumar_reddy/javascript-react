//? promises are used to handle asynch operations in javascript
//? Promise is an object ,that represent the eventual completion or failure  of an asynchronous operations.

//  Things to learn in promises:

//!  What is a Event loop ?
//?  Event loop in a javascript is a single threaded mechanism, that manages the  execution of multiple  tasks , including asynchronous operations such as callbacks, Promises , and I/O operations
setTimeout(() => {
  console.log("displays after 4 seconds ");
}, 2000); //?  hi  , how are you   , display after 4 seconds
console.log("hi");
console.log("how ara you");

//! What is inversion of control in callbacks?
//?  invertion of control is nothing but losing control on the program flow , by passing  function as argument to another function .
function ioc(callback) {
  setTimeout(() => {
    const data = "hi";
    callback(data);
  }, 3000);
}
ioc((data) => {
  console.log(data);
});

//!   What are browser API's?
//?    Browser APIs are utilized for DOM manipulation, asynchronous communication with servers, and storage mechanisms, enabling dynamic web page interactions,
//?         data fetching, and local data storage in web applications.

//!   What is a Promise ?
//?   Promise is an object that represent Eventual completion or failure of an  asynchronous operations.
//? In promise there are 3 stated   1 . pending -- Initial stage
//?                                 2 . resolve -- indicates the promise is successful
//?                                 3 . reject -- indicates the promise is unsuccessful

//! How to create a new promise ?
//?      To create a new Promise in JavaScript, use the Promise constructor and pass a function with resolve and reject parameters.
//?         Inside this function, perform your asynchronous operation, then call resolve with a value when it's successful, or reject with an error if it fails.
//?     The then() method is called when the promise is resolved, ​and the catch() method is called if the promise is rejected or if there was an error during the code execution

//? we can create 3 types of promise
// resolved promise---------------------------------------------------------------------------------------------------------------------------------------
let promise1 = Promise.resolve("promise  is resolved ");
promise1.then((result) => {
  console.log(result);
});
// reject promise -----------------------------------------------------------------------------------------------------------------------------------------
let promise2 = Promise.reject(" promise is rejected");
promise2.catch((result) => {
  console.log(result);
});

let promise3 = new Promise((resolve, reject) => {
  // Code to perform the promised task
  let task_performed = true;
  if (task_performed) {
    resolve("The promised task was performed successfully.");
  } else {
    reject("The promised task was not performed.");
  }
});
promise3
  .then((result) => console.log(result))
  .catch((error) => console.log(error));

//!   What are different states of a Promise - pending, fulfilled, rejected
//? In promise there are 3 stated   1 . pending -- Initial stage
//?                                 2 . resolve -- indicates the promise is successful
//?                                 3 . reject -- indicates the promise is unsuccessful

//!How to consume an existing promise ?
//? The then() method is used to handle the fulfillment of the promise.
//?  The catch() method is used to handle the rejection of the promise.

//! How to chain promises using .then

// Example of chaining promises

// Assume we have a function that returns a promise
function asyncTask1() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("Result of asyncTask1");
    }, 1000);
  });
}

// Another function that returns a promise
function asyncTask2(data) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(`Result of asyncTask2 with data: ${data}`);
    }, 1000);
  });
}

// Chain promises using then()
asyncTask1()
  .then((result1) => {
    console.log(result1); // Output: Result of asyncTask1
    return asyncTask2(result1); // Return a promise from the first then() handler
  })
  .then((result2) => {
    console.log(result2); // Output: Result of asyncTask2 with data: Result of asyncTask1
  })
  .catch((error) => {
    console.error("Error:", error); // Handle any errors in the chain
  });
//---------------------------------------------------------------------------------------------------------------------------------------

//!  How to handle errors in a promise chain using .catch
// Example of handling promise chain errors with catch

// Assume we have a function that returns a promise
function asyncTask1() {
  return new Promise((resolve, reject) => {
    // Simulating an asynchronous operation
    setTimeout(() => {
      resolve("Result of asyncTask1");
    }, 1000);
  });
}

// Another function that returns a promise
function asyncTask2(data) {
  return new Promise((resolve, reject) => {
    // Simulating an asynchronous operation that throws an error
    setTimeout(() => {
      reject("Error in asyncTask2");
    }, 1000);
  });
}

// Chain promises and handle errors with catch
asyncTask1()
  .then((result1) => {
    console.log(result1); // Output: Result of asyncTask1
    return asyncTask2(result1); // Return a promise from the first then() handler
  })
  .then((result2) => {
    console.log(result2); // This will not be executed since asyncTask2 rejected the promise
  })
  .catch((error) => {
    console.error("Error caught:", error); // Handle any errors in the chain
  });

//!   finally block in a promise chain
// Example of using finally() in a promise chain

// Assume we have a function that returns a promise
function asyncTask() {
  return new Promise((resolve, reject) => {
    // Simulating an asynchronous operation
    setTimeout(() => {
      resolve("Result of asyncTask");
    }, 1000);
  });
}

// Chain promises and use finally() to perform cleanup
asyncTask()
  .then((result) => {
    console.log(result); // Output: Result of asyncTask
  })
  .catch((error) => {
    console.error("Error caught:", error); // Handle any errors in the chain
  })
  .finally(() => {
    console.log(
      "Cleanup code here, it will print  if the promise is  resolve or rejected"
    ); // Execute cleanup code regardless of promise outcome
  });

//!What happens when an Error gets thrown inside .then when there is a .catch
//?  When an error occurs inside a .then() method of a promise chain and there is a .catch() method attached to the chain, the error will be caught by the .catch() handler.

//! What happens when an Error gets thrown inside .then when there is no .catch
//? When an error occurs inside a .then() method of a promise chain and there is no .catch() method attached to the chain, the error will result in an unhandled promise rejection.
// Example of error handling inside a promise chain with no .catch()

// Assume we have a function that returns a promise
function asyncTask() {
  return new Promise((resolve, reject) => {
    // Simulating an asynchronous operation that throws an error
    setTimeout(() => {
      reject("Error in asyncTask");
    }, 1000);
  });
}

// Chain promises with no .catch()
asyncTask().then((result) => {
  // This code will not be executed since asyncTask rejects the promise
  console.log("Result:", result);
});

//!  Why must .catch be placed towards the end of the promise chain?-
//?  Error Handling Scope: By placing .catch() towards the end of the chain, you can handle errors from multiple .then() methods in one centralized location.
//?                         This improves code readability and maintainability by keeping error handling logic separate from other asynchronous operations.
//?  Chain Continuity: Placing .catch() at the end ensures that the promise chain continues to execute even if an error occurs.
//?                     If you place .catch() earlier in the chain, it would terminate the chain as soon as an error is encountered, preventing subsequent .then() methods from executing.

//!   How to consume multiple promises by chaining?
//?    To consume multiple promises by chaining them together, you can use the .then() method to chain promises sequentially,
//?       ensuring that each subsequent promise waits for the previous one to resolve before executing. Here's how you can do it:
// Assume we have three functions that return promises
function asyncTask1() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("Result of asyncTask1");
    }, 1000);
  });
}

function asyncTask2(data) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(`Result of asyncTask2 with data: ${data}`);
    }, 1000);
  });
}

function asyncTask3(data) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(`Result of asyncTask3 with data: ${data}`);
    }, 1000);
  });
}

// Chain promises to consume multiple promises
asyncTask1()
  .then((result1) => {
    console.log(result1); // Output: Result of asyncTask1
    return asyncTask2(result1);
  })
  .then((result2) => {
    console.log(result2); // Output: Result of asyncTask2 with data: Result of asyncTask1
    return asyncTask3(result2);
  })
  .then((result3) => {
    console.log(result3); // Output: Result of asyncTask3 with data: Result of asyncTask2 with data: Result of asyncTask1
  })
  .catch((error) => {
    console.error("Error caught:", error); // Handle any errors in the chain
  });

//!   How to do error handling when using promises?
//?  Error handling with promises can be done using the .catch() method or by attaching a rejection handler to individual promises using the .then() method.
//?       Here's how you can handle errors when using promises:

//?  1:  Using .catch() Method: You can append a .catch() method at the end of the promise chain to catch any errors that occur in any part of the chain.
myPromise
  .then((result) => {
    // Handle the resolved value
  })
  .catch((error) => {
    // Handle any errors that occurred in the promise chain
  });

//?  2: Attaching Rejection Handlers: You can also handle errors for individual promises by attaching a rejection handler using the second parameter of the .then() method.

myPromise.then(
  (result) => {
    // Handle the resolved value
  },
  (error) => {
    // Handle any errors that occurred in this specific promise
  }
);
//?  3:  Error Propagation: Errors propagate down the promise chain until they are caught by a .catch() method or a rejection handler.
//?         You can use multiple .catch() methods or rejection handlers at different points in the chain to handle errors appropriately.
myPromise
  .then((result) => {
    // Handle the resolved value
    return anotherPromise;
  })
  .then((result) => {
    // Handle the result of anotherPromise
  })
  .catch((error) => {
    // Handle any errors in the promise chain
  });

//?      Global Error Handling: Ensure that you have a global error handler to catch any unhandled promise rejections.
//?           This can be done using process.on('unhandledRejection') in Node.js or window.addEventListener('unhandledrejection') in browsers.
process.on("unhandledRejection", (error) => {
  console.error("Unhandled promise rejection:", error);
});

//!   Why is error handling the most important part of using a promise?
//?     Prevents Uncaught Errors:  ,  Maintains Code Stability: ,  Enhances Debugging:  ,  Improves User Experience:  ,  Facilitates Error Recovery:

//!   How to promisify an asynchronous callbacks based function - eg. setTimeout, fs.readFile
//?    Promisifying setTimeout:

function delay(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

// Usage:
delay(1000).then(() => {
  console.log("Delayed operation executed after 1 second");
});
//----------------------------------------------------------------------------------------
//?  Promisifying fs.readFile:

const fs = require("fs");

function readFileAsync(path, options) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, options, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

// Usage:
readFileAsync("example.txt", "utf8")
  .then((data) => {
    console.log("File content:", data);
  })
  .catch((err) => {
    console.error("Error reading file:", err);
  });

//!How to use the following promise based functions
/*
Promise.resolve
Promise.reject
Promise.all
Promise.allSettled
Promise.any
Promise.race

*/

//   promise.all

//?   Promise.all() is a method in JavaScript that takes an array of promises as input and returns a single Promise.
//?        This new Promise will resolve when all the input promises have resolved, or reject immediately if any of the input promises reject.

//?           --> You provide an array of promises to Promise.all().
//?            --> It returns a new Promise.
//?            --> If all the input promises resolve successfully, the new Promise returned by Promise.all() will resolve with an array of their resolved values, in the same order as the input promises.
//?            --> If any of the input promises reject, the new Promise returned by Promise.all() will reject with the reason of the first rejected promise encountered.
const promise4 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Promise 1 resolved");
  }, 1000);
});

const promise5 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Promise 2 resolved");
  }, 2000);
});

const promise6 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Promise 3 resolved");
  }, 3000);
});

Promise.all([promise4, promise5, promise6])
  .then((values) => {
    console.log("All promises resolved:", values);
  })
  .catch((error) => {
    console.error("Error:", error);
  });

//?   failure cases
//todo   --> Immediate Rejection: If any of the input promises to Promise.all() reject, the resulting promise returned by Promise.all() will immediately reject with the reason of the first rejected promise encountered.
//todo                           This means that the rejection will occur as soon as one of the promises rejects, without waiting for the other promises to settle.

//todo  --> Rejection Handling: Always attach a .catch() method after Promise.all() to handle any rejections that may occur.
//todo                          This ensures that you catch and handle errors gracefully, preventing unhandled promise rejections.

//todo  --> Handling Partial Results: Keep in mind that when one of the promises rejects, the other promises may still be pending or may have already resolved.
//todo                                 Depending on your use case, you may need to handle partial results or clean up any resources associated with the resolved promises.

//todo  --> Order of Results: The order of results in the array passed to the .then() method after Promise.all() corresponds to the order of the input promises.
//todo                         If one of the promises rejects, the corresponding position in the results array will be undefined.

//todo  --> Error Propagation: Errors in promises passed to Promise.all() propagate to the resulting promise returned by Promise.all().
//todo                         Ensure that you handle errors appropriately to prevent them from propagating further and causing unintended behavior in your application.


//-------------------------------------------------------------------------------------------------------------------------------------------
// Promise.allSettled

//?  Promise.allSettled() is a method in JavaScript that takes an array of promises as input and returns a single Promise. 
//?     This new Promise will always resolve, once all the input promises have settled (either fulfilled or rejected), with an array of objects representing the outcome of each promise

//?   You provide an array of promises to Promise.allSettled().
//?   It returns a new Promise.
//?   This new Promise will resolve once all the input promises have settled (either fulfilled or rejected).
//?   The resolved value is an array of objects, where each object represents the outcome of each promise
//?           If the promise is fulfilled, the object contains a status of "fulfilled" and a value property with the fulfilled value.
//?           If the promise is rejected, the object contains a status of "rejected" and a reason property with the rejection reason.
const promise7 = Promise.resolve('Resolved promise');
const promise8 = Promise.reject('Rejected promise');
const promise9 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('Another resolved promise');
  }, 2000);
});

Promise.allSettled([promise7, promise8, promise9])
  .then((results) => {
    results.forEach((result, index) => {
      if (result.status === 'fulfilled') {
        console.log(`Promise ${index + 1} fulfilled with value:`, result.value);
      } else {
        console.error(`Promise ${index + 1} rejected with reason:`, result.reason);
      }
    });  
  });

//todo     Multiple API Requests: You need to fetch data from multiple APIs, and you want to handle both successful and failed requests. With Promise.allSettled(), 
//todo                             you can wait for all requests to complete and then process each response individually, regardless of whether they succeeded or failed.

//todo      Database Operations: You're performing multiple database operations (e.g., inserts, updates, deletes) and want to ensure that all operations are completed before proceeding. 
//todo                            Promise.allSettled() allows you to track the status of each operation and handle any errors gracefully.
 
//todo     Batch Processing: You're processing a batch of tasks asynchronously, such as resizing images or processing files. With Promise.allSettled(), 
//todo                        you can process the entire batch and receive a summary of the results, including any failures that occurred during processing.

//todo     Batch Processing: You're processing a batch of tasks asynchronously, such as resizing images or processing files. With Promise.allSettled(), 
//todo                        you can process the entire batch and receive a summary of the results, including any failures that occurred during processing.

//todo    Validation Checks: You're performing multiple validation checks on user input or data from an external source.
//todo             Promise.allSettled() enables you to run all validation checks concurrently and receive a comprehensive report of the validation results, including any validation errors encountered.



//--------------------------------------------------------------------------------

//  Promise.race

 //?   Promise.race() is a method in JavaScript that takes an iterable of promises as input and returns a single Promise.
//?           This new Promise resolves or rejects as soon as one of the input promises resolves or rejects, with the value or reason of that promise.



//?        -->  You provide an iterable (such as an array) of promises to Promise.race().
//?        -->It returns a new Promise.
//?        -->This new Promise resolves or rejects as soon as one of the input promises settles (either fulfills or rejects).
//?        -->If the first settled promise is fulfilled, the new Promise resolves with the value of that promise.
//?        -->If the first settled promise is rejected, the new Promise rejects with the reason of that promise.

const promise11 = new Promise((resolve, reject) => {
   setTimeout(() => {
     resolve('Promise 1 resolved');
   }, 2000);
 });
 
 const promise12 = new Promise((resolve, reject) => {
   setTimeout(() => {
     reject('Promise 2 rejected');
   }, 1000);
 });
 
 Promise.race([promise11, promise12])
   .then((value) => {
     console.log('First promise settled:', value);
   })
   .catch((reason) => {
     console.error('First promise rejected with reason:', reason);    // output:    First promise rejected with reason: Promise 2 rejected
   });
 
   //todo    Failure-cases

//todo      Timeout Scenarios: Promise.race() is commonly used in scenarios involving timeouts, where you want to race between a promise and a timeout promise.
//todo                          If the operation represented by the promise takes longer than expected, you can handle the timeout as a rejection from Promise.race().
                                   

//todo      Fastest Promise Wins: Promise.race() resolves or rejects as soon as the first promise in the iterable settles (either fulfills or rejects). 
//todo                      If a promise rejects before others fulfill, the returned promise will reject with the reason of that first-rejected promise.
   
//todo      No Wait for Others: Once the first promise settles, Promise.race() does not wait for other promises in the race to settle. 
//todo                        This means that any subsequent resolutions or rejections of other promises in the race are ignored.

//todo      Unhandled Rejections: If the first settling promise is a rejection and there is no .catch() or .then() method to handle it,
//todo                         the rejection becomes an unhandled promise rejection. It's essential to handle or catch such rejections to prevent unhandled promise rejection error
   
// todo     Handling Multiple Failures: If multiple promises reject simultaneously or very close to each other, only the first rejection is captured by Promise.race(). 
//todo                    Subsequent rejections are not considered, so it's important to handle potential failures appropriately based on the specific requirements of your application.

//----------------------------------------------------------------------------------------------------------------------------

// Promise.any()

//?       Promise.any() is a method in JavaScript that takes an iterable of promises as input and returns a single Promise. 
//?         This new Promise resolves as soon as any of the input promises fulfills, with the value of the first fulfilled promise. If all input promises reject, Promise.any() returns a rejected Promise with an AggregateError containing all rejection reasons.



//?  -->  You provide an iterable (such as an array) of promises to Promise.any().
//?  -->  It returns a new Promise.
//?  -->  This new Promise resolves as soon as any of the input promises fulfills.
//?  -->  If one of the input promises fulfills, the new Promise resolves with the value of that promise.
//?  -->  If all input promises reject, the new Promise rejects with an AggregateError containing all rejection reasons.

const promise13 = new Promise((resolve, reject) => {
   setTimeout(() => {
     resolve('Promise 1 resolved');
   }, 2000);
 });
 
 const promise14 = new Promise((resolve, reject) => {
   setTimeout(() => {
     reject('Promise 2 rejected');
   }, 1000);
 });
 
 const promise15 = new Promise((resolve, reject) => {
   setTimeout(() => {
     resolve('Promise 3 resolved');
   }, 3000);
 });
 
 Promise.any([promise13, promise14, promise15])
   .then((value) => {
     console.log('First promise fulfilled:', value);
   })
   .catch((error) => {
     console.error('All promises rejected:', error);        //output  :  promise 1 is resolved
   });
 
//!   How to consume multiple promises by Promise.all?
