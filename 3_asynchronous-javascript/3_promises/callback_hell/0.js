// // const promise1 = Promise.resolve('Resolved promise');
// // const promise2 = Promise.reject('Rejected promise');
// // const promise3 = new Promise((resolve, reject) => {
// //   setTimeout(() => {
// //     resolve('Another resolved promise');
// //   }, 5000);
// // });

// // Promise.allSettled([promise1, promise2, promise3])
// //   .then((results) => {
// //     results.forEach((result, index) => {
// //       if (result.status === 'fulfilled') {
// //         console.log(`Promise ${index + 1} fulfilled with value:`, result.value);
// //       } else {
// //         console.error(`Promise ${index + 1} rejected with reason:`, result.reason);
// //       }
// //     });
// //   });



// const promise1 = new Promise((resolve, reject) => {
//     setTimeout(() => {
//       resolve('Promise 1 resolved');
//     }, 2000);
//   });
  
//   const promise2 = new Promise((resolve, reject) => {
//     setTimeout(() => {
//       reject('Promise 2 rejected');
//     }, 1000);
//   });
  
//   Promise.race([promise1, promise2])
//     .then((value) => {
//       console.log('First promise settled:', value);
//     })
//     .catch((reason) => {
//       console.error('First promise rejected with reason:', reason);
//     });
  




//     //?   Promise.race() is a method in JavaScript that takes an iterable of promises as input and returns a single Promise.
//     //?        This new Promise resolves or rejects as soon as one of the input promises resolves or rejects, with the value or reason of that promise.


const promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Promise 1 resolved');
    }, 2000);
  });
  
  const promise2 = new Promise((resolve, reject) => {
    setTimeout(() => {
      reject('Promise 2 rejected');
    }, 1000);
  });
  
  const promise3 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Promise 3 resolved');
    }, 3000);
  });
  
  Promise.any([promise1, promise2, promise3])
    .then((value) => {
      console.log('First promise fulfilled:', value);
    })
    .catch((error) => {
      console.error('All promises rejected:', error);        //output  :  promise 1 is resolved
    });
  