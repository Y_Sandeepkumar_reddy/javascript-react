// fetch("https://pokeapi.co/api/v2/pokemon/pikachu")
//   .then((response)=>console.log(response))
//   .catch((error)=>console.log(error));

//! here we get the data from the url    now next task is  to convert it into json format

fetch("https://pokeapi.co/api/v2/pokemon/pikachu")
  .then((response) => response.json())
  .then((data) => {
    console.log(data);
  })
  .catch((error) => console.log(error));

//!  here we converted the data into json



//! now using async   await function

async function fetchdat() {
    try {
        const res= await fetch("https://pokeapi.co/api/v2/pokemon/typhlosion")
        if(!response.ok){
            throw new Error("could not fetch resource")
        }
        const data= await response.json()
        console.log(data);

    } catch (error) {
         console.log(error);
    }
}


