//!            What is a callback function?

//?          --> The  function takes another function as a parameter and calls it inside. This is valid in JavaScript and we call it a “callback”.  
//?                So a function that is passed to another function as a parameter is a callback function 

//!            How do you define a callback function in JavaScript?
function calculateSum(n, callback) {
    var i;
    var sum = 0;
    for (i = 1; i <= n; i++) {
      sum += i;
    }
    console.log(`Sum is: ${sum}`);
    callback(sum);
  }
  
  var n = 10;
  calculateSum(n, function(sum){
    console.log(`Average is: ${sum/n}`);
  });

//!              What is the purpose of using callback functions?
//?              The purpose of using callback functions is to run the code asynchronously and block the main thread.  
//?                    This allows the program to continue executing other code while waiting for a specific task to complete.


//!            Explain the difference between synchronous and asynchronous callback functions.

//?  1.  In JavaScript, synchronous functions are those that execute one operation at a time, in the order they are called.
//?      The code execution is blocked until the function completes its task and returns a value.
//?        On the other hand, asynchronous functions allow the code to continue executing while the function's task is being processed. 
//?          This is particularly useful when dealing with time-consuming operations, such as network requests or file I/O, as it prevents the program from being blocked and unresponsive during these operations.


//?  2. Callback functions can be used in both synchronous and asynchronous contexts. A synchronous callback function is executed immediately,
//?           before the calling function continues. In contrast, an asynchronous callback function is executed at a later time, after the calling function has completed its execution.


//!         What is a higher-order function? How does it relate to callback functions?

//?      A higher-order function is a function that accepts other functions as arguments or returns functions as results. 
//?          Callback functions are often used as arguments to higher-order functions.

//!        How do you handle errors with callback functions??
//?    In JavaScript, errors can be handled within callback functions using an error-first callback approach. 
//?       This approach involves passing an additional argument to the callback function, which is used to handle any errors that may occur during the execution of the asynchronous operation.

// Asynchronous function with callback
function asyncFunction(input, callback) {
    // Simulate an asynchronous operation (e.g., fetching data from a server)
    setTimeout(function() {
        // Simulate an error condition if input is less than 5
        const error = input < 5 ? new Error('Input parameter is too small') : null;

        // Call the callback function with the error (if any) as the first argument
        if (error) {
            callback(error, null); // Pass the error
        } else {
            const result = input * 2; // Calculate result
            callback(null, result); // Pass the result
        }
    }, 1000);
}

// Usage of the asynchronous function with error handling
asyncFunction(3, function(error, result) {
    if (error) {
        console.error('Error:', error.message);
    } else {
        console.log('Result:', result);
    }
});




//!      What are the limitations or drawbacks of using callback functions?

//?      Discuss potential issues such as callback hell, readability, and difficulty in managing asynchronous control flow.


//!      How can you avoid callback hell?

//?      Discuss techniques such as modularization, named functions, Promises, async/await, and libraries like async.js to mitigate the problem of deeply nested callback functions.