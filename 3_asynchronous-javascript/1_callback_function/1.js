/* 
 The  function takes another function as a parameter and calls it inside. This is valid in JavaScript and we call it a “callback”.  
    So a function that is passed to another function as a parameter is a callback function 

*/
function calculateSum(n, callback) {
    var i;
    var sum = 0;
    for (i = 1; i <= n; i++) {
      sum += i;
    }
    console.log(`Sum is: ${sum}`);
    callback(sum);
  }
  
  var n = 10;
  calculateSum(n, function(sum){
    console.log(`Average is: ${sum/n}`);
  });

/*  --> JavaScript runs code sequentially in top-down order. However, there are some cases that code runs (or must run) after something else happens and also not sequentially. 
      This is called asynchronous programming.

    --> Callbacks make sure that a function is not going to run before a task is completed but will run right after the task has completed.
       It helps us develop asynchronous JavaScript code and keeps us safe from problems and errors.

    --> In JavaScript, the way to create a callback function is to pass it as a parameter to another function, and then to call it back right after something has happened or some task is completed. Let’s see how…
*/


//! How to create call back function
const message = function() {
    console.log("This message is shown after 3 seconds");
}

setTimeout(message, 3000);

/*

--> There is a built-in method in JavaScript called “setTimeout”, which calls a function or evaluates an expression after a given period of time (in milliseconds).
      So here, the “message” function is being called after 3 seconds have passed. (1 second = 1000 milliseconds)

--> In other words, the message function is being called after something happened (after 3 seconds passed for this example), 
      but not before. So the message function is an example of a callback function.
*/
setTimeout(function() {
    console.log("This annomous function is shown after 4 seconds");
}, 4000);

setTimeout(() => {
    console.log("This  Arrow function is shown after 5 seconds");
}, 5000);