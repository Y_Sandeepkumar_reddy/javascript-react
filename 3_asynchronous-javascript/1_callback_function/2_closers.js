//?  A closure is a JavaScript feature that allows a function to retain access to variables from its parent scope even after the parent function has finished executing.
//?        A closer gives you to access to  anouter function's scope from inner function. In javascript closers are created every time  a function is created , at function creation time  

function outerFunction() {
    let outerVariable = 'I am from outer function';

    function innerFunction() {
        console.log(outerVariable); // Access outerVariable from outer function
    }

    return innerFunction;
}

const innerFunc = outerFunction();
innerFunc(); // Logs: I am from outer function
