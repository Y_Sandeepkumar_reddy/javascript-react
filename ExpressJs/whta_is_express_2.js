//! Express Middle-Ware

//?  -->  "Middle-Ware" functions are the  functions  that have access to the "request" and "response" object.
//?        Express has built in middleware  but middleware also comes from 3rd party packages as well as custom middleware

//? --> Execute any code
//? --> Makes changes to the request/response object
//? --> End response cycle
//? --> Call next middleware in the stack


//!   NOW WORKING ON   USING REAL TIME   
//!    to install express   frist   " npm init -y "  ,  "npm i express"   next set   "start": "node index.js" in package.json file  , create index.js file  and start working on index.js file
   
    const express=require('express');

    const app=express();
    
    // app.listen() is a method used to start a server and listen for incoming connections on a specified port. This method is typically used with frameworks like Express.js.
    const PORT=process.env.PORT || 5000
    
    app.listen(PORT,()=>console.log(`Server started on port ${PORT}`))