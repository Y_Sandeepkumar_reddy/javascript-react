//!   What is Exprerss.Js    Why it is used
//? Express Js is most  popular  Web framework  for node 

//?      Express is fast,unopionionated and minimalist web framework for Node.js
//?      Express is a layer built on the top on the NOde.js that helps manage a server and routs
//?      Express is a npm package.


//?   -->  Express is a "server-side" or "back-end" framework. It is not compatable  to Clint-side  framework like React, Angular js, Vve.
//?           it can be used in combination with those framework  to build full Stack applications

//!  Why  use express ?

//?  --> Makes building web applications with Node.js  MUCH easier
//?  --> Used for both server rendered apps as well as API/Microservers
//? --> Extreamly light , fast and free
//? --> full control of request and respond
//? --> By far most populat Node framework
//? --> Great to use with clint side frameworks as it's all JavaScript


//! WHAT WE HAVE TO KNOW FRIST

//?  --> JavaScript Fundamentals  (Objects, Arrays,Conditionals,etc)
//?  --> Basic Node.js & Npm

//?   --> It may help to learn these first
 //?      1. HTTP Status Codes    2. JSON    3. High Order Array Methods -forEach,map, filter   4. Arraow Functions

 //!    Basic server Syntax

 const express=require('express')
 // Init express
 const app=express();

 // create your end points and /route handlers
 app.get('/',function(req,res){
    res.send('hello world !');
 });

 // listen on a port
 app.listen(5000);

 //! BASIC ROUTE HANDLING
 //?   -->   hANDLING REQUESTS/ROUTES IS SIMPLE
 //?   -->   app.get(), app.post(), app.delete(), app.put(), app.patch(), app.options(), app.head() .
 //?   -->   Access to params, Query strings, url parts, etc
 //?   -->   Expresss has a router so we can store routes in seperates files and exports
 //?   -->   We can parse incoming data with the Body Parser

 //?  1. app.get() - This method is used to handle GET requests. It's used when you want to retrieve data from the server.
 //?  2. app.post() - This method is used to handle POST requests. It's used when you want to submit data to the server, typically to create new resources.
 //?  3. app.delete() - This method is used to handle DELETE requests. It's used when you want to delete a resource from the server.
 //?  4. app.put() - This method is used to handle PUT requests. It's used when you want to update a resource on the server. The entire resource is replaced with the new data.
 //?  5. app.patch() - This method is used to handle PATCH requests. It's used when you want to partially update a resource on the server. Unlike PUT, which replaces the entire resource, PATCH is used to apply partial modifications.
 //?  6. app.options() - This method is used to handle OPTIONS requests. It's used to describe the communication options for the target resource, such as which HTTP methods are allowed on that resource.
 //?  7. app.head() - This method is used to handle HEAD requests. It's similar to a GET request, but the server only returns the HTTP headers without the actual content of the resource. It's often used to check for the existence of a resource or to retrieve metadata about a resource without downloading the entire content.


 //? 1. res.send() - is a method used in Express.js to send a response to the client. It's typically used to send a string, JSON, buffer, or an HTML file back to the client as part of an HTTP response.
 const express = require('express');
const app = express();

app.get('/', (req, res) => {
  res.send('Hello, world!');
});

app.listen(3000, () => {
  console.log('Server is running on port 3000');
});
// In this example, when a GET request is made to the root URL ("/"), the server responds with the string "Hello, world!" using the res.send() method.

//?  You can also send JSON data:
app.get('/json', (req, res) => {
    res.send({ message: 'Hello, JSON!' });
  });
// In this case, when a GET request is made to the "/json" endpoint, the server responds with a JSON object { message: 'Hello, JSON!' }.



 //-----------------------------------------------------------------------------------------------
//?  res.json() - This method is used to send a JSON response to the client. It automatically sets the Content-Type header to application/json. 

 app.get('/json', (req, res) => {
    res.json({ message: 'Hello, JSON!' });
  });


//?  res.sendFile() - This method is used to send a file as the response. It automatically sets the Content-Type header based on the file extension.
  app.get('/file', (req, res) => {
    res.sendFile('/path/to/file');
  });

  
//?      res.render() - This method is used to render a view template using a templating engine like EJS, Handlebars, or Pug.
app.get('/template', (req, res) => {
    res.render('template', { title: 'Template Example' });
  });

  
//?     res.status() - This method is used to set the HTTP status code of the response.
  app.get('/error', (req, res) => {
    res.status(404).send('Not Found');
  });
  
