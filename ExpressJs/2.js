//!                         REST API      middle ware


const members = [
    {
      id: 1,
      name: 'John Doe',
      email: 'john@gmail.com',
      status: 'active'
    },
    {
      id: 2,
      name: 'Bob Williams',
      email: 'bob@gmail.com',
      status: 'inactive'
    },
    {
      id: 3,
      name: 'Shannon Jackson',
      email: 'shannon@gmail.com',
      status: 'active'
    }
  ];
  
  module.exports = members;





const express=require('express');
const path=require('path')
const members=require("./members")
const app=express();


const logger=((req,res,next)=>{
    console.log(`${req.protocol}://${req.get('host')}${req.originalUrl}`);
    next();
});
app.use(logger)
//Get all the members
app.get('/api/members',(req,res)=>res.json(members))

// set static folder
app.use(express.static(path.join(__dirname,'public')))
// use is a method  that we use only when we want to include middleware in our application

const PORT=process.env.PORT || 5000

// app.listen() is a method used to start a server and listen for incoming connections on a specified port. This method is typically used with frameworks like Express.js.
app.listen(PORT,()=>console.log(`Server started on port ${PORT}`))



 //?  when we run http://localhost:5000/api/members/   this url it will show in the terminal     it shows which url user gets(need)

 //todo      NOW WE ARE ADDING DATE AND TIME  TO THIS 
//?  for that we are using moment     install moment    "npm i moment"

const express=require('express');
const path=require('path')
const members=require("./members")
const moment=require('moment');
const app=express();


const logger=((req,res,next)=>{
    console.log(`${req.protocol}://${req.get('host')}${req.originalUrl}: ${moment().format()}`);
    next();
});
app.use(logger)
//Get all the members
app.get('/api/members',(req,res)=>res.json(members))

// set static folder
app.use(express.static(path.join(__dirname,'public')))
// use is a method  that we use only when we want to include middleware in our application

const PORT=process.env.PORT || 5000

// app.listen() is a method used to start a server and listen for incoming connections on a specified port. This method is typically used with frameworks like Express.js.
app.listen(PORT,()=>console.log(`Server started on port ${PORT}`))
//?   the above gives the url and time  