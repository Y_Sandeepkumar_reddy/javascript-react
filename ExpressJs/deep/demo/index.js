const express=require('express');
const path=require('path')
const members=require("./members")
const moment=require('moment');
const app=express();


const logger=((req,res,next)=>{
    console.log(`${req.protocol}://${req.get('host')}${req.originalUrl}: ${moment().format()}`);
    next();
});
app.use(logger)
//Get all the members
app.get('/api/members',(req,res)=>res.json(members))

// set static folder
app.use(express.static(path.join(__dirname,'public')))
// use is a method  that we use only when we want to include middleware in our application

const PORT=process.env.PORT || 5000

// app.listen() is a method used to start a server and listen for incoming connections on a specified port. This method is typically used with frameworks like Express.js.
app.listen(PORT,()=>console.log(`Server started on port ${PORT}`))