//! creating server and  listning for specific content in a specific port

const express=require('express');

const app=express();

// app.listen() is a method used to start a server and listen for incoming connections on a specified port. This method is typically used with frameworks like Express.js.

app.get("/",(req,res)=>{
    res.send("helo world , what can i do for you")
})
app.get("/1",(req,res)=>{
    res.send(`<h1>helo world , what can i do for you<h1>`)
})



const PORT=process.env.PORT || 5000

app.listen(PORT,()=>console.log(`Server started on port ${PORT}`))



//?  to seee source code use like this       view-source:localhost:5000

//?  EVERY TIME WE ARE RESTARTING SERVER  BUT IT IS DIFICULT , SO  I AM INSTALLING NODEMON MODULE  
//?        npm i -D nodemon    -  D is shorter version of --save-dev
//?        set in packge.json "dev":"nodemon index"  the nodempon constantly abserve


