//!                             STATIC SERVER


//! index.html file   is  created inside public -> index.html
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
    <h1 style="color: aqua;"><b>Welcome to sandeep's world</b></h1>
    <input type="text" placeholder="enter your name">
</body>
</html>

//!  index.js file

const express=require('express');
const path=require('path')

const app=express();

// app.listen() is a method used to start a server and listen for incoming connections on a specified port. This method is typically used with frameworks like Express.js.

app.get("/",(req,res)=>{
    res.sendFile(path.join(__dirname,'public','index.html'))
})

const PORT=process.env.PORT || 5000

app.listen(PORT,()=>console.log(`Server started on port ${PORT}`))





//!  the  above is not the actual Why   we have to  do correct way


//!    we have to set static web folder
// static folder must be public
// html file 1 in  public folder  
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
    <h1>about  me</h1>
</body>
</html>

// file 2 in public folder

<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
    <h1 style="color: aqua;"><b>Welcome to sandeep's world</b></h1>
    <input type="text" placeholder="enter your name">
</body>
</html>

// index.js  file

const express=require('express');
const path=require('path')

const app=express();


// set static folder
app.use(express.static(path.join(__dirname,'public')))
// use is a method  that we use only when we want to include middleware in our application

const PORT=process.env.PORT || 5000

// app.listen() is a method used to start a server and listen for incoming connections on a specified port. This method is typically used with frameworks like Express.js.
app.listen(PORT,()=>console.log(`Server started on port ${PORT}`))

//?   when we run this using npm run dev   serverv start at local port:5000 and  if wew extand with file_name1  that data in that content p displays, if we give second_filename that content will displays extension is ,must.
